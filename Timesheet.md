# Timesheet

Please use this table to provide the total working time per deliverable. This means that you have to sum the working hours of each component of the team.

| Deliverable | Total working time in hours |
|:-----------:|:------------------:|
|Requirements| 80 |
|Design | 40 |
|Coding |  82 |
|Testing | 124 |
