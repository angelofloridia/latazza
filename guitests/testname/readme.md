# LaTazza GUI tests
This is a small readme file to explain how we interpreted GUI tests.
## What we have done

* For the gui test we had to write a script that initializes the application values.

* So each test initially calls init_general.txt which only acts the first time, if instead it has already been called it should not produce any action.
* **[Important]** To make the tests work and call each other correctly it is necessary to put all the scripts in the EyeAutomate scripts folder and the photos in the images folder.

* We also want to underline how changing computers, sometimes the tests on the GUI *do not work*, in particular on the closing of the windows. This we have seen to be a problem often encountered even on the internet.