package it.polito.latazza.data;

public class Beverage {
	Integer id;
	String name;
	Integer capsulePrice;
	Integer BoxPrice;
	Integer capsulesPerBox;
	Integer numberOfCapsules;
	Integer newNumberOfCapsules;
	Integer newBoxPrice;
	
	public Beverage(Integer id, String name, Integer capsulesPerBox, Integer BoxPrice){
		this.id=id;
		this.name=name;
		this.capsulesPerBox=capsulesPerBox;
		this.BoxPrice=BoxPrice;
		this.numberOfCapsules=0;
		this.capsulePrice = this.BoxPrice / this.capsulesPerBox;
		this.newBoxPrice=0;
		this.newNumberOfCapsules=0;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getBoxPrice() {
		
		return BoxPrice;
	}
	
	public void setBoxPrice(Integer boxPrice) {
		BoxPrice = boxPrice;
	}

	public Integer getCapsulesPerBox() {
		return capsulesPerBox;
	}
	
	public void setCapsulesPerBox(Integer capsulesPerBox) {
		this.capsulesPerBox = capsulesPerBox;
	}

	public Integer getNumberOfCapsules() {
		return numberOfCapsules;
	}

	public void setNumberOfCapsules(Integer numberOfCapsules) {
		this.numberOfCapsules = numberOfCapsules;
	}

	public Integer getCapsulePrice() {
		return capsulePrice;
	}

	public void setCapsulePrice(Integer capsulePrice) {
		this.capsulePrice = capsulePrice;
	}

	public Integer getNewNumberOfCapsules() {
		return newNumberOfCapsules;
	}

	public void setNewNumberOfCapsules(Integer newNumberOfCapsules) {
		this.newNumberOfCapsules = newNumberOfCapsules;
	}

	public Integer getNewBoxPrice() {
		return newBoxPrice;
	}

	public void setNewBoxPrice(Integer newBoxPrice) {
		this.newBoxPrice = newBoxPrice;
	}
}