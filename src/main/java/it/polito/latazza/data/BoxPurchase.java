package it.polito.latazza.data;

import java.text.SimpleDateFormat;
import java.util.Date;

import it.polito.latazza.exceptions.BeverageException;

public class BoxPurchase extends Transaction{
	
	Integer quantity;
	Integer beverageId;
	
	public BoxPurchase(Integer id, Integer amount, Integer beverageId, Integer quantity) {
		super(id, amount);
		this.beverageId=beverageId;
		this.quantity=quantity;
	}
	

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}


	public Integer getBeverageId() {
		return beverageId;
	}
	
	public String toString()
	{
		String toReturn = new String();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		toReturn += sdf.format(data) + " BUY ";						
		try {
			toReturn += LaTazza.getInstance().getInventoryManager().getBeverageName(beverageId) + " " + quantity;
		} catch (BeverageException e) {
			e.printStackTrace();
		}
		//System.out.println(toReturn);
		return toReturn;
	}
}
