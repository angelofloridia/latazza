package it.polito.latazza.data;

import java.text.SimpleDateFormat;
import java.util.Date;

import it.polito.latazza.exceptions.BeverageException;
import it.polito.latazza.exceptions.EmployeeException;

public class Consumption extends Transaction{
	
	private Integer employee;
	private Integer beverageId;
	private Integer quantity;
	private boolean cashFlag;	
	
	
	public Consumption(Integer id, Integer amount, Integer quantity, boolean flag,Integer empl,Integer beverage_id) {
		super(id, amount);
		this.cashFlag=flag;
		this.employee=empl;
		this.quantity = quantity;
		this.beverageId=beverage_id;
	}
	

	public boolean getCashFlag() {
		return this.cashFlag;
	}
	public void setCashFlag(boolean flag) {
		this.cashFlag=flag;
	}
	
	public String toString()
	{
		String toReturn = new String();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		toReturn += sdf.format(data) + " ";
		if (employee == null) {
			toReturn += "VISITOR "; 
		}
		else
		{
			String empl_s = null;
			try {
				empl_s = LaTazza.getInstance().getEmployeeManager().getEmployeeById(employee).toString();
			} catch (EmployeeException e) {
				e.printStackTrace();
				System.exit(-1);
			}
			if (cashFlag)
			{
				toReturn += "CASH " + empl_s + " ";				
			}
			else
			{
				toReturn += "BALANCE " + empl_s + " ";
			}
			
		}
				
		try {
			toReturn += LaTazza.getInstance().getInventoryManager().getBeverageName(beverageId) + " ";
		} catch (BeverageException e1) {
			e1.printStackTrace();
			System.exit(-1);
		}		
		toReturn += quantity;				
		return toReturn;
	}
	
	public Integer getEmployee() {
		return employee;
	}
	public void setEmployee(Integer employee) {
		this.employee = employee;
	}
	public Integer getBeverageId() {
		return beverageId;
	}
	public void setBeverageId(Integer beverageId) {
		this.beverageId = beverageId;
	}
	
	
}
