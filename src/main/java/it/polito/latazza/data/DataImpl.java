package it.polito.latazza.data;

import java.util.Date;
import java.util.List;
import java.util.Map;

import it.polito.latazza.data.database.Database;
import it.polito.latazza.exceptions.BeverageException;
import it.polito.latazza.exceptions.DateException;
import it.polito.latazza.exceptions.EmployeeException;
import it.polito.latazza.exceptions.NotEnoughBalance;
import it.polito.latazza.exceptions.NotEnoughCapsules;

public class DataImpl implements DataInterface {
	
	private InventoryManager inv;
	private EmployeeManager empl;
	private TransactionManager trn;
	
	
	public DataImpl() {
		
		
		Database.firstConnect(); //prova a collegarsi al DB, se non esiste ne crea uno vuoto.		
		inv=LaTazza.getInstance().getInventoryManager();
		empl=LaTazza.getInstance().getEmployeeManager();
		trn=LaTazza.getInstance().getTransactionManager();
	
		
	}
	
	@Override
	public Integer sellCapsules(Integer employeeId, Integer beverageId, Integer numberOfCapsules, Boolean fromAccount)
			throws EmployeeException, BeverageException, NotEnoughCapsules {
		return trn.atomicSellCapsules(employeeId, beverageId, numberOfCapsules, fromAccount);
	}

	@Override
	public void sellCapsulesToVisitor(Integer beverageId, Integer numberOfCapsules)
			throws BeverageException, NotEnoughCapsules {
		trn.sellCapsulesToVisitor(beverageId, numberOfCapsules);		
	}

	@Override
	public Integer rechargeAccount(Integer id, Integer amountInCents) throws EmployeeException {
		return trn.rechargeAccount(id, amountInCents);
	}

	@Override
	public void buyBoxes(Integer beverageId, Integer boxQuantity) throws BeverageException, NotEnoughBalance {
		trn.buyBoxes(beverageId, boxQuantity);
		
	}

	@Override
	public List<String> getEmployeeReport(Integer employeeId, Date startDate, Date endDate)
			throws EmployeeException, DateException {
		return LaTazza.getInstance().getReportManager().getEmployeeReport(employeeId, startDate, endDate);
	}

	@Override
	public List<String> getReport(Date startDate, Date endDate) throws DateException {
		return LaTazza.getInstance().getReportManager().getReport(startDate, endDate);
	}

	@Override
	public Integer createBeverage(String name, Integer capsulesPerBox, Integer boxPrice) throws BeverageException {
		return inv.createBeverage(name, capsulesPerBox, boxPrice);
	}

	@Override
	public void updateBeverage(Integer id, String name, Integer capsulesPerBox, Integer boxPrice)
			throws BeverageException {
		inv.updateBeverage(id, name, capsulesPerBox, boxPrice);
	}

	@Override
	public String getBeverageName(Integer id) throws BeverageException {
		return inv.getBeverageName(id);
	}

	@Override
	public Integer getBeverageCapsulesPerBox(Integer id) throws BeverageException {
		return inv.getBeverageCapsulesPerBox(id);
	}

	@Override
	public Integer getBeverageBoxPrice(Integer id) throws BeverageException {
		Beverage tmp = inv.getBeverageById(id);
		
		if(tmp == null)
			throw new BeverageException();
		
		if(tmp.getNewBoxPrice() == 0)
			return inv.getBeverageBoxPrice(id);
		else
			return tmp.getNewBoxPrice();
	}

	@Override
	public List<Integer> getBeveragesId() {
		return inv.getBeveragesId();
	}

	@Override
	public Map<Integer, String> getBeverages() {
		return inv.getBeverages();
	}

	@Override
	public Integer getBeverageCapsules(Integer id) throws BeverageException {
		Beverage tmp = inv.getBeverageById(id);
		if(tmp == null)
			throw new BeverageException();
		return inv.getBeverageCapsules(id) +tmp.getNewNumberOfCapsules();
	}

	@Override
	public Integer createEmployee(String name, String surname) throws EmployeeException {
		return empl.createEmployee(name, surname);
	}

	@Override
	public void updateEmployee(Integer id, String name, String surname) throws EmployeeException {		
		empl.updateEmployee(id, name, surname);
	}

	@Override
	public String getEmployeeName(Integer id) throws EmployeeException {		
		return empl.getEmployeeName(id);	
	}

	@Override
	public String getEmployeeSurname(Integer id) throws EmployeeException {
		return empl.getEmployeeSurname(id);
	}

	@Override
	public Integer getEmployeeBalance(Integer id) throws EmployeeException {		
		return empl.getEmployeeBalance(id);
	}

	@Override
	public List<Integer> getEmployeesId() {
		return empl.getEmployeesId();
	}

	@Override
	public Map<Integer, String> getEmployees() {
		return empl.getEmployees();
	}

	@Override
	public Integer getBalance() {
		return LaTazza.getInstance().getBalance();
	}

	@Override
	public void reset() {		
		LaTazza.reset();
		inv=LaTazza.getInstance().getInventoryManager();
		empl=LaTazza.getInstance().getEmployeeManager();
		trn=LaTazza.getInstance().getTransactionManager();
	}

}
