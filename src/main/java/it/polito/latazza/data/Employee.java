package it.polito.latazza.data;

import java.util.HashSet;

import it.polito.latazza.data.database.Database;

public class Employee {

	private Integer id;
	private String name;
	private String surname;
	private Integer balance;
	HashSet<Consumption> consumptionSet;
	HashSet<Recharge> rechargeSet;
	
	public Employee(String name, String surname, Integer id)
	{
		this.name = name;
		this.surname = surname;
		this.id = id;
		this.balance = 0;
		consumptionSet = new HashSet<Consumption>();
		rechargeSet = new HashSet<Recharge>();
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getSurname()
	{
		return surname;
	}
	
	public void setSurname(String surname)
	{
		this.surname = surname;
	}
	
	public Integer getBalance()
	{
		return balance;
	}
	
	public Integer getId()
	{
		return id;
	}
	
	public void setBalance(Integer balance)
	{
		this.balance = balance;
		Database.updateEmployeeBalance(this.id, balance);
	}
		
	public void recordConsumption(Consumption cons)
	{
		if (cons != null) {
			consumptionSet.add(cons);			
		}	
	}
	
	public void recordRecharge(Recharge recharge)
	{
		if (recharge != null) {
			rechargeSet.add(recharge);	
		}			
	}
	
	public String toString()
	{
		String toReturn = new String();
		toReturn += name + " " + surname;
		return toReturn;
	}
	
	public HashSet<Consumption> getConsumptions()
	{
		return consumptionSet;
	}
	
	public HashSet<Recharge> getRecharges()
	{
		return rechargeSet;
	}
	
}
