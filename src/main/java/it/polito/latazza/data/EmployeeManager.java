package it.polito.latazza.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.polito.latazza.data.database.Database;
import it.polito.latazza.exceptions.EmployeeException;


public class EmployeeManager {

	private Map<Integer, Employee> employeeMap;
	int employeeCount;
	
	public EmployeeManager()
	{
		employeeMap = new HashMap<Integer, Employee>();
		employeeMap = Database.getAllEmployees();
		employeeCount = employeeMap.size();
	}
	
	Integer createEmployee(String name, String surname) throws EmployeeException
	{	
		if(name==null||surname==null || name=="" || surname=="") {
			throw new EmployeeException();
		}
		if (getEmployeeByName(name, surname) != null)
		{
			throw new EmployeeException();
		}
		Employee employee = new Employee(name, surname, employeeCount);		
		employeeMap.put(employeeCount, employee);
		Database.createEmployee(employee);
		return employeeCount++;
	}
	
	public void updateEmployee(Integer id, String name, String surname) throws EmployeeException
	{		
		Employee empl = employeeMap.get(id);
		if (empl == null)
		{
			throw new EmployeeException();
		}
		if (name == null || name == "" || surname == null || surname == "")
			throw new EmployeeException();
		empl.setName(name);
		empl.setSurname(surname);
	}
	
	public String getEmployeeName(Integer id) throws EmployeeException
	{
		Employee empl = employeeMap.get(id);
		if (empl == null)
		{
			throw new EmployeeException();
		}	
		return empl.getName();
	}
	
	public String getEmployeeSurname(Integer id) throws EmployeeException
	{
		Employee empl = employeeMap.get(id);
		if (empl == null)
		{
			throw new EmployeeException();
		}	
		return empl.getSurname();
	}
	
	public Integer getEmployeeBalance(Integer id) throws EmployeeException
	{
		Employee empl = employeeMap.get(id);
		if (empl == null)
		{
			throw new EmployeeException();
		}	
		return empl.getBalance();
	}			
	
	public void recordConsumption(Integer employeeId, Consumption consumption)
	{
		employeeMap.get(employeeId).recordConsumption(consumption);		
	}
	
	public void recordRecharge(Integer employeeId, Recharge recharge)
	{
		employeeMap.get(employeeId).recordRecharge(recharge);		
	}
	
	private Employee getEmployeeByName(String name, String surname)
	{		
		for (Map.Entry<Integer, Employee> entry : employeeMap.entrySet())
		{
			Employee emp = entry.getValue();
			if (emp.getName().equals(name) &&
					emp.getSurname().equals(surname))
			{
				return emp;
			}
		}
		return null;
	}
	
	public Employee getEmployeeById(Integer id) throws EmployeeException
	{
		if (id == null) {
			return null;
		}
		
		Employee empl = employeeMap.get(id);
		if (empl == null)
		{
			throw new EmployeeException();
		}
		return empl;
	}
	
	public List<Integer> getEmployeesId() {

		ArrayList<Integer> toReturn = new ArrayList<Integer>();
		for (Map.Entry<Integer, Employee> entry : employeeMap.entrySet())
		{			
			toReturn.add(entry.getKey());
		}		
		return toReturn;
	}
	
	public Map<Integer, String> getEmployees() { 

		HashMap<Integer, String> toReturn = new HashMap<Integer, String>();
		for (Map.Entry<Integer, Employee> entry : employeeMap.entrySet())
		{
			Employee emp = entry.getValue();
			String str = emp.getName() + " " + emp.getSurname();
			toReturn.put(entry.getKey(), str);
		}
		return toReturn;
	}
	
	
	
}