package it.polito.latazza.data;
import java.util.*;
import java.util.stream.Collectors;

import it.polito.latazza.data.database.Database;
import it.polito.latazza.exceptions.BeverageException;

public class InventoryManager {
	
	private Map<Integer, Beverage> beverages;
	private Integer n;
	
	public InventoryManager() {
		beverages = new HashMap<Integer, Beverage>();
		beverages = Database.getAllBeverages();
		n = beverages.size();
	}
	
	public Integer createBeverage(String name, Integer capsulesPerBox, Integer boxPrice) throws BeverageException {
		if(name==null||name=="")
			throw new BeverageException();
		if(capsulesPerBox==null || capsulesPerBox<0||boxPrice==null||boxPrice<0)
			throw new BeverageException();
		n++;
		Beverage bev = this.beverages.get(n);
		if (bev != null)
			throw new BeverageException();
		bev = new Beverage(n, name, capsulesPerBox, boxPrice);
		beverages.put(n, bev);
		//database.create something
		Database.createBeverage(bev);
		return n;
	}
	
	public void updateBeverage(Integer id, String name, Integer capsulesPerBox, Integer boxPrice) throws BeverageException {
		if(beverages.get(id)==null || name == null || name=="" || capsulesPerBox == null ||capsulesPerBox<=0 || boxPrice == null|| boxPrice<=0) {
			throw new BeverageException();
		}
		
		Beverage tmp = this.beverages.get(id);
		tmp.setName(name);
		tmp.setCapsulesPerBox(capsulesPerBox);
		
		if(tmp.getBoxPrice() != boxPrice) {
			tmp.setNewBoxPrice(boxPrice);
		}else {
			tmp.setBoxPrice(boxPrice);
		}
		
		Database.updateBeverage(id, name, capsulesPerBox, boxPrice, 0, tmp.getNewBoxPrice(), tmp.getNewNumberOfCapsules());
	}
	
	public String getBeverageName(Integer id) throws BeverageException {
		if(this.beverages.get(id)==null)
			throw new BeverageException();
		return beverages.get(id).getName();
	}
	
	public Integer getBeverageCapsulesPerBox(Integer id) throws BeverageException {
		if(this.beverages.get(id)==null)
			throw new BeverageException();
		return beverages.get(id).getCapsulesPerBox();
	}
	
	public Integer getBeverageBoxPrice(Integer id) throws BeverageException {
		if(this.beverages.get(id)==null)
			throw new BeverageException();
		return beverages.get(id).getBoxPrice();
	}
	
	public List<Integer> getBeveragesId(){
		return new ArrayList<Integer>(this.beverages.keySet());
	}
	
	public Map<Integer, String> getBeverages(){ 		//da testare lo stream
		return this.beverages.values().stream().collect(Collectors.toMap(Beverage :: getId, Beverage :: getName));
	}
	
	public Integer getBeverageCapsules(Integer id) throws BeverageException{
		//quando si cambia questo, si deve cambiare anche il resto, prima, col metodo updateBeverage.
		//questo commento è per quanto riguarda l'implementazione nel database
		if(this.beverages.get(id)==null)
			throw new BeverageException();
		return this.beverages.get(id).getNumberOfCapsules();
	}

	public void setBeverageCapsules(Integer id, Integer quantity) throws BeverageException{
		Beverage tmp = this.beverages.get(id);
		if(this.beverages.get(id)==null)
			throw new BeverageException();
		
		this.beverages.get(id).setNumberOfCapsules(quantity);
		Database.updateBeverage(id, tmp.getName(), tmp.getCapsulesPerBox(), tmp.getBoxPrice(), quantity, tmp.getNewBoxPrice(), tmp.getNewNumberOfCapsules());
	}
	
	//È un metodo che passato come parametro il tipo di beverage Ti ritorna quante bevande ci sono di quel tipo
	//io e Lorenzo abbiamo ipotizzato che il tipo fosse dato dal nome stesso.
	public Long getBeverageByType(String name) {
		return this.beverages.values().stream()
				.filter(e -> e.getName() == name)
				.collect(Collectors.counting());
	}
	
	public Integer getCapsulePrice(Integer id){
	    return this.beverages.get(id).getCapsulePrice();
	}

	public Beverage getBeverageById(Integer id) {
		return this.beverages.get(id);
	}
	
}
