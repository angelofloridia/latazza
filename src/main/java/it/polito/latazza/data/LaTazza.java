package it.polito.latazza.data;

import it.polito.latazza.data.database.Database;

public class LaTazza {

	static private LaTazza instance;
	private int balance;
	private InventoryManager inventoryMan;
	private EmployeeManager employeeMan;
	private TransactionManager transactionMan;
	private ReportManager reportMan;
	
	private LaTazza()
	{
		instance = this;
		balance = 0;
		inventoryMan = new InventoryManager();
		employeeMan = new EmployeeManager();
		transactionMan = new TransactionManager();
		reportMan = new ReportManager();
	}
	
	public static LaTazza getInstance()
	{
		if (instance == null)
		{
			instance = new LaTazza();			
		}
		return instance;
	}
	
	public InventoryManager getInventoryManager()
	{
		return inventoryMan;
	}
	
	public EmployeeManager getEmployeeManager()
	{
		return employeeMan;
	}
	
	public TransactionManager getTransactionManager()
	{
		return transactionMan;
	}

	public ReportManager getReportManager()
	{
		return reportMan;
	}
	
	public Integer getBalance()
	{
		return balance;
	}
	
	public void setBalance(Integer balance)
	{
		this.balance = balance;
	}
	
	public static void reset()
	{
		Database.resetDatabase();
		instance = new LaTazza();		
	}
	
}
