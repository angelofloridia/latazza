package it.polito.latazza.data;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import it.polito.latazza.exceptions.EmployeeException;

public class Recharge extends Transaction {
	Integer employee;
	public Recharge(Integer id, Integer amount,Integer e) {
		super(id, amount);
		this.employee=e;
		
	}
	


	public String toString()
	{
		String toReturn = new String();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		toReturn += sdf.format(data) + " ";
		toReturn += "RECHARGE ";
		String empl_s = null;
		try {
			empl_s = LaTazza.getInstance().getEmployeeManager().getEmployeeById(employee).toString() + " ";
		} catch (EmployeeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
		toReturn += empl_s;
		double amount_d = (double) amount;
		toReturn += String.format(Locale.US, "%.2f \u20ac", amount_d/100);	
		return toReturn;
	}

	public Integer getEmployee() {
		return employee;
	}

	public void setEmployee(Integer employee) {
		this.employee = employee;
	}
	
	
	
}

