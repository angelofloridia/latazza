package it.polito.latazza.data;

import it.polito.latazza.exceptions.DateException;
import it.polito.latazza.exceptions.EmployeeException;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class ReportManager {

	List<String> getEmployeeReport(Integer employeeId, Date startDate, Date endDate)
												throws EmployeeException, DateException
	{
		ArrayList<Transaction> list=new ArrayList<Transaction>();
		
		if (startDate == null || endDate == null || startDate.compareTo(endDate) > 0) {
			throw new DateException();
		}
		
		if(employeeId == null)
			throw new EmployeeException();
		
		ArrayList<String> toReturn = new ArrayList<String>();
		Employee empl = LaTazza.getInstance().getEmployeeManager().getEmployeeById(employeeId);
		
		for (Consumption c : empl.getConsumptions())
		{
			list.add(c);
		}
		for (Recharge r : empl.getRecharges())
		{	
			list.add(r);
		}
		Collections.sort(list);
		for(Transaction t : list) {
			if(t instanceof Consumption) {
				Date data = t.getData();
				if (data.compareTo(startDate) >= 0 &&
						data.compareTo(endDate) <= 0)
				{
					toReturn.add(t.toString());
				}
			}
			if(t instanceof Recharge) {
				Date data = t.getData();
				if (data.compareTo(startDate) >= 0 &&
						data.compareTo(endDate) <= 0)
				{
					toReturn.add(t.toString());
				}
			}
		}
		
		
		
		return toReturn;
	}
	
	public List<String> getReport(Date startDate, Date endDate) throws DateException
	{	if(startDate==null||endDate==null)
			throw new DateException();
		if (startDate.compareTo(endDate) > 0) {
			throw new DateException();
		}
		
		Map<Integer, Transaction> transactions = LaTazza.getInstance().getTransactionManager().getTransactions();
		ArrayList<String> toReturn = new ArrayList<>();
		for (Map.Entry<Integer, Transaction> entry : transactions.entrySet())
		{
			Date data = entry.getValue().getData();
			if (data.compareTo(startDate) >= 0 &&
					data.compareTo(endDate) <= 0)
			{
				toReturn.add(entry.getValue().toString());
			}
		}				
		return toReturn;
	}
	
	
	
}
