package it.polito.latazza.data;

import java.util.Comparator;
import java.util.Date;


public class Transaction implements Comparable<Transaction> {
	
	protected Integer id, amount;
	protected Date data;
	
	public Transaction(Integer id,Integer amount){
		this.data=new Date();
		this.id=id;
		this.amount=amount;
				
	}
		
	
	public Integer getId() {
		return this.id;
	}
	public Date getData() {
		return this.data;
	}
	public Integer getAmount() {
		return this.amount;
	}
	public void setAmount(Integer new_amount) {
		this.amount=new_amount;
	}
	public void setData(Date new_data) {
		this.data=new_data;
	}


	@Override
	public int compareTo(Transaction arg1) {
		if(this.getData().before(arg1.getData())) {
			return -1;
		}
		else if(this.getData().after(arg1.getData())) {
			return 1;
		}
		else
			return 0;
	}
}
