package it.polito.latazza.data;

import java.util.HashMap;
import java.util.Map;

import it.polito.latazza.data.database.Database;
import it.polito.latazza.exceptions.BeverageException;
import it.polito.latazza.exceptions.EmployeeException;
import it.polito.latazza.exceptions.NotEnoughBalance;
import it.polito.latazza.exceptions.NotEnoughCapsules;



public class TransactionManager {
	private Map<Integer,Transaction> transactions;
	
	public TransactionManager() {
		transactions = new HashMap<Integer,Transaction>();
		transactions = Database.getAllTransactions();
	};
	
	public Integer atomicSellCapsules(Integer employeeId, Integer beverageId, Integer numberOfCapsules, Boolean fromAccount)
			throws EmployeeException, BeverageException, NotEnoughCapsules {
		
		InventoryManager inv = LaTazza.getInstance().getInventoryManager();
		Beverage bev = inv.getBeverageById(beverageId);
		Integer remainingCapsules = inv.getBeverageCapsules(beverageId);
		Integer newNumberOfCapsules = bev.getNewNumberOfCapsules();
		
		if(numberOfCapsules == null || numberOfCapsules < 0)
			throw new NotEnoughCapsules();
		
		if(remainingCapsules+newNumberOfCapsules < numberOfCapsules) {
			throw new NotEnoughCapsules();			
		}
		
		if(numberOfCapsules <= remainingCapsules) {
			return sellCapsules(employeeId, beverageId, numberOfCapsules, fromAccount);
		}
		else {
			if(remainingCapsules != 0 ) {
				sellCapsules(employeeId, beverageId, remainingCapsules, fromAccount);
			}
			bev.setBoxPrice(bev.getNewBoxPrice());
			bev.setNumberOfCapsules(bev.getNewNumberOfCapsules());
			bev.setNewBoxPrice(0);
			bev.setNewNumberOfCapsules(0);
			bev.setCapsulePrice(bev.getBoxPrice()/bev.getCapsulesPerBox());
			return sellCapsules(employeeId, beverageId, numberOfCapsules - remainingCapsules, fromAccount);
		}
	}
	

	public Integer sellCapsules(Integer employeeId, Integer beverageId, Integer numberOfCapsules, Boolean fromAccount)
			throws EmployeeException, BeverageException, NotEnoughCapsules {
		
		EmployeeManager empl = LaTazza.getInstance().getEmployeeManager();
		
		InventoryManager inv = LaTazza.getInstance().getInventoryManager();
		
		Beverage tmp = inv.getBeverageById(beverageId);

		if(numberOfCapsules == null || numberOfCapsules < 0)
			throw new NotEnoughCapsules();
		Integer amount = empl.getEmployeeBalance(employeeId);
		Integer n_prima = inv.getBeverageCapsules(beverageId);
		if(n_prima - numberOfCapsules < 0)
			throw new NotEnoughCapsules();
		Integer n_dopo = n_prima - numberOfCapsules;
		inv.setBeverageCapsules(beverageId, n_dopo);
		Database.updateBeverage(beverageId, inv.getBeverageName(beverageId), inv.getBeverageCapsulesPerBox(beverageId), inv.getBeverageBoxPrice(beverageId), n_dopo, tmp.getNewBoxPrice(), tmp.getNewNumberOfCapsules());
		Integer new_amount;
		
		Consumption c;
		if(fromAccount == true) {
			
			new_amount = amount - inv.getCapsulePrice(beverageId)*numberOfCapsules;
			
			empl.getEmployeeById(employeeId).setBalance(new_amount);			
			c = new Consumption(transactions.size()+1, inv.getCapsulePrice(beverageId)*numberOfCapsules, numberOfCapsules, false, employeeId, beverageId);
			transactions.put(transactions.size()+1,c);
		
		}
		else {
			new_amount = amount;
			c = new Consumption(transactions.size()+1, inv.getCapsulePrice(beverageId)*numberOfCapsules, numberOfCapsules, true, employeeId,beverageId);
			transactions.put(transactions.size()+1, c);
			Database.updateLaTazzaBalance(inv.getCapsulePrice(beverageId)*numberOfCapsules);
			LaTazza.getInstance().setBalance(LaTazza.getInstance().getBalance()+inv.getCapsulePrice(beverageId)*numberOfCapsules);
		}
		empl.recordConsumption(employeeId, c);
		Database.createConsumption(c);
	
		return new_amount;
	}
	
	
	
	
	public void sellCapsulesToVisitor(Integer beverageId, Integer numberOfCapsules)
			throws BeverageException, NotEnoughCapsules {
		if(numberOfCapsules==null) {
			throw new NotEnoughCapsules();
		}
		if (numberOfCapsules < 0)
			throw new NotEnoughCapsules();
		InventoryManager inv = LaTazza.getInstance().getInventoryManager();
		if (inv == null)
			throw new BeverageException();
		Beverage bev = inv.getBeverageById(beverageId);
		Integer n_prima=inv.getBeverageCapsules(beverageId);
		if(n_prima-numberOfCapsules < 0)
			throw new NotEnoughCapsules();
		if(numberOfCapsules<0)
			throw new BeverageException();
		Integer n_dopo=n_prima-numberOfCapsules;
		inv.setBeverageCapsules(beverageId, n_dopo);
		Database.updateBeverage(beverageId, inv.getBeverageName(beverageId), inv.getBeverageCapsulesPerBox(beverageId), inv.getBeverageBoxPrice(beverageId), n_dopo, bev.getNewBoxPrice(), bev.getNewNumberOfCapsules());
		Integer tmp= inv.getCapsulePrice(beverageId)*numberOfCapsules;
		Consumption c=new Consumption(transactions.size()+1, tmp, numberOfCapsules, true, null, beverageId);
		transactions.put(transactions.size()+1,c);
		Integer amount=LaTazza.getInstance().getBalance();
		Integer new_amount=amount+(inv.getCapsulePrice(beverageId)*numberOfCapsules);
		LaTazza.getInstance().setBalance(new_amount);
		Database.updateLaTazzaBalance(new_amount);
		
		/******************DA VERIFICARE***********/
		Database.createConsumption(c);
		/*******************/
	}
	
	
	
	
	public Integer rechargeAccount(Integer id, Integer amountInCents) throws EmployeeException {
		if(amountInCents==null || amountInCents<=0)
			throw new EmployeeException();
		EmployeeManager empl = LaTazza.getInstance().getEmployeeManager();
		if(empl == null)
			throw new EmployeeException();
		if(empl.getEmployeeById(id)==null)
			throw new EmployeeException();
		Integer amount = empl.getEmployeeBalance(id);
		Integer new_amount = amount+amountInCents;
		empl.getEmployeeById(id).setBalance(new_amount);
		Recharge r = new Recharge(transactions.size()+1, new_amount, id);
		transactions.put(transactions.size()+1, r);	
		empl.recordRecharge(id, r);
		Database.createRecharge(r);
		LaTazza.getInstance().setBalance(LaTazza.getInstance().getBalance()+amountInCents);
		Database.updateLaTazzaBalance(LaTazza.getInstance().getBalance());
		return new_amount;
		//commento di prova 
	}
	
	
	
	
	
	public void buyBoxes(Integer beverageId, Integer boxQuantity) throws BeverageException, NotEnoughBalance {

		InventoryManager inv = LaTazza.getInstance().getInventoryManager();
		if(inv == null)
			throw new BeverageException();
		
		Beverage tmp = inv.getBeverageById(beverageId);
		if(tmp == null)
			throw new BeverageException();
		if(boxQuantity<0) {
			throw new BeverageException();
		}
		Integer price = 0;
		if(tmp.getNewBoxPrice() == 0) {
			price = tmp.getBoxPrice();
		}else {
			price = tmp.getNewBoxPrice();
		}
			
		BoxPurchase b = new BoxPurchase(transactions.size()+1, price*boxQuantity, beverageId, boxQuantity);		
		transactions.put(transactions.size()+1, b);
		
		Integer n_prima=inv.getBeverageCapsules(beverageId);
		Integer n_dopo=n_prima+boxQuantity*inv.getBeverageCapsulesPerBox(beverageId);
		
		Integer amount=LaTazza.getInstance().getBalance();
		Integer new_amount=amount-(price*boxQuantity);
		if(new_amount < 0)
			throw new NotEnoughBalance();
		
		if(tmp.getNewBoxPrice() != 0) {
			tmp.setNewNumberOfCapsules(boxQuantity*inv.getBeverageCapsulesPerBox(beverageId));			
		}else {
			tmp.setNumberOfCapsules(n_dopo);
		}
		
		LaTazza.getInstance().setBalance(new_amount);
		Database.updateLaTazzaBalance(new_amount);
		Database.createBoxPurchase(b);
	}
	
	public Map<Integer, Transaction> getTransactions()
	{
		return transactions;
	}
	
}
