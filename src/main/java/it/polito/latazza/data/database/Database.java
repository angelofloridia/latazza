package it.polito.latazza.data.database;

import java.sql.Connection; 
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException; 
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import it.polito.latazza.data.Beverage;
import it.polito.latazza.data.BoxPurchase;
import it.polito.latazza.data.Consumption;
import it.polito.latazza.data.Employee;
import it.polito.latazza.data.Recharge;
import it.polito.latazza.data.Transaction;  

public class Database { 
   // JDBC driver name and database URL 
   static final String JDBC_DRIVER = "org.h2.Driver";   
   static final String DB_URL = "jdbc:h2:~/test";  
   /*cambiamenti per evitare l'errore su git:
    * Togliere IFEXISTS=TRUE
    * nel metodo firstConnect chiamare in ogni caso createDatabase 
    */
   
   //  Database credentials 
   static final String USER = "sa"; 
   static final String PASS = ""; 
  
   /* BASE METHODS */
   public static Connection firstConnect() { 
	      Connection conn = null; 
	      try { 
	         // STEP 1: Register JDBC driver 
	         Class.forName(JDBC_DRIVER); 
	             
	         //STEP 2: Open a connection 
	         System.out.println("Connecting to database for first time..."); 
	         
	         conn = DriverManager.getConnection(DB_URL,USER,PASS);      
	         
	         Database.createDatabase();
	         	         
	      } catch(Exception e) { 
	         System.out.println("New empty Database created."); 
	      } 
	      
	      return conn;
   } 
   
   public static boolean LaTazzaCheck() { 
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   String sql = "SELECT * FROM LATAZZA";
		   stmt.executeUpdate(sql);
		   System.out.println(sql);
		   
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         Database.createLaTazzaTable(); 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }

	   } 
   
   public static Connection connect() { 
      Connection conn = null; 
      try { 
         // STEP 1: Register JDBC driver 
         Class.forName(JDBC_DRIVER); 
             
         //STEP 2: Open a connection 
         System.out.println("Connecting to database..."); 
         
         conn = DriverManager.getConnection(DB_URL,USER,PASS);    
         
      
      } catch(SQLException se) { 
         //Handle errors for JDBC 
         se.printStackTrace(); 
      } catch(Exception e) { 
         //Handle errors for Class.forName 
         e.printStackTrace(); 
      } 
      
      return conn;
   } 
   
   public static boolean createDatabase() {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   
		   String sql1 = "CREATE TABLE IF NOT EXISTS Employee (id INT PRIMARY KEY, name VARCHAR(255), surname VARCHAR(255), balance INT);";
		   stmt.executeUpdate(sql1);
		   System.out.println(sql1);
		   
		   String sql2 = "CREATE TABLE IF NOT EXISTS Beverage (id INT PRIMARY KEY, name VARCHAR(255), capsulesPerBox INT, boxPrice INT, numberOfCapsules INT, newBoxPrice INT, newNumberOfCapsules INT);";
		   stmt.executeUpdate(sql2);
		   System.out.println(sql2);
		   
		   String sql3= "CREATE TABLE IF NOT EXISTS Transaction (id INT PRIMARY KEY, amount INT, data VARCHAR(255), type VARCHAR(1), employee_id INT, cashflag BOOLEAN, beverage_id INT, quantity INT);";
		   stmt.executeUpdate(sql3);
		   System.out.println(sql3);
		   
		   Database.LaTazzaCheck();
		   
		   
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         se.printStackTrace(); 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
	   
   }
   
   public static boolean createEmployeeTable() {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   
		   String sql1 = "CREATE TABLE IF NOT EXISTS Employee (id INT PRIMARY KEY, name VARCHAR(255), surname VARCHAR(255), balance INT);";
		   stmt.executeUpdate(sql1);
		   System.out.println(sql1);
		   
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         se.printStackTrace(); 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
	   
   }
   
   public static boolean createBeverageTable() {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   
		   String sql2 = "CREATE TABLE IF NOT EXISTS Beverage (id INT PRIMARY KEY, name VARCHAR(255), capsulesPerBox INT, boxPrice INT, numberOfCapsules INT,  newBoxPrice INT, newNumberOfCapsules INT);";
		   stmt.executeUpdate(sql2);
		   System.out.println(sql2);
		   
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         se.printStackTrace(); 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
	   
   }
   
   public static boolean createTransactionTable() {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   
		   String sql3= "CREATE TABLE IF NOT EXISTS Transaction (id INT PRIMARY KEY, amount INT, data VARCHAR(255), type VARCHAR(1), employee_id INT, cashflag BOOLEAN, beverage_id INT, quantity INT);";
		   stmt.executeUpdate(sql3);
		   System.out.println(sql3);
		   
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         se.printStackTrace(); 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
	   
   }
   
   public static boolean createLaTazzaTable() {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   String sql = "CREATE TABLE IF NOT EXISTS LaTazza (id INT PRIMARY KEY, balance INT);";
		   stmt.executeUpdate(sql);
		   System.out.println(sql);
		   
		   Database.insertNewLaTazza();
		   
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	          
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
	   
   }
   
   public static boolean insertNewLaTazza() {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   
		   String sql4 = "INSERT INTO LATAZZA VALUES (0,0);";
		   stmt.executeUpdate(sql4);
		   System.out.println(sql4);
		   
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
	   
   }
   
  
   
   public static boolean resetDatabase() {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   String sql = "DELETE FROM LaTazza;";
		   stmt.executeUpdate(sql);
		   System.out.println(sql);
		   
		   String sql1 = "DELETE FROM Employee;";
		   stmt.executeUpdate(sql1);
		   System.out.println(sql1);
		   
		   String sql2 = "DELETE FROM Beverage;";
		   stmt.executeUpdate(sql2);
		   System.out.println(sql2);
		   
		   String sql3= "DELETE FROM Transaction;";
		   stmt.executeUpdate(sql3);
		   System.out.println(sql3);
		   
		   
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) {  
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
	   
   }
   
   
   /*	METHODS USED FOR BUSINESS RULES		*/
   public static boolean createEmployee(Employee employee) {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   String sql = "INSERT INTO Employee VALUES(" + employee.getId() +", '"+ employee.getName() +"', '" + employee.getSurname() + "', 0)";
		   stmt.executeUpdate(sql);
		   System.out.println(sql);
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         se.printStackTrace(); 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
   }
   
   public static boolean updateEmployee ( Integer id, String name, String surname) {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   String sql = "UPDATE Employee SET NAME  ='" + name +"', SURNAME = '" + surname + "' WHERE ID = " + id;
		   System.out.println(sql);
		   stmt.executeUpdate(sql);
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         se.printStackTrace(); 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
   }
   
   public static boolean updateEmployeeBalance ( Integer id, Integer balance) {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   String sql = "UPDATE Employee SET BALANCE  =" + balance +" WHERE ID = " + id;
		   System.out.println(sql);
		   stmt.executeUpdate(sql);
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         se.printStackTrace(); 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
   }
   
  /* public Employee getEmployee ( Integer id) {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   String sql = "SELECT * FROM EMPLOYEE WHERE ID =" + id;
		   System.out.println(sql);
		   stmt.executeUpdate(sql);
		   ResultSet rs = stmt.executeQuery(sql); 
		   Employee e = null;
		   while(rs.next()) { 
		          // Retrieve by column name 
			   	  
		          int idE  = rs.getInt("id"); 
		          String name = rs.getString("name"); 
		          String surname = rs.getString("surname");
		          int balance = rs.getInt("balance"); 
		          e = new Employee(name, surname, idE);
		          e.setBalance(balance);
		       } 
		   rs.close(); 
		   stmt.close(); 
	       conn.close(); 
	       return e;
	      } catch(SQLException se) { 
	       	         se.printStackTrace(); 
	       	         return null;
	      } catch(Exception ex) { 
	       	         ex.printStackTrace(); 
	       	         return null;
	      }
   }
   	 */
   public static boolean createBeverage(Beverage bev) {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   String sql = "INSERT INTO Beverage VALUES(" + bev.getId() +
				   ", '"+ bev.getName() +"', " 
				   + bev.getCapsulesPerBox()+ ", " 
				   +bev.getBoxPrice()+ ", " 
				   + bev.getNumberOfCapsules() + ", "
				   + bev.getNewBoxPrice()+ ", "
				   + bev.getNewNumberOfCapsules() + ")";
		   
		   stmt.executeUpdate(sql);
		   System.out.println(sql);
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         se.printStackTrace(); 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
   }
   
   public static boolean updateBeverage ( Integer id, String name, Integer capsulesPerBox, Integer boxPrice, Integer numberOfCapsules, Integer newBoxPrice, Integer newNumberOfCapsules) {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   String sql = "UPDATE Beverage SET NAME  ='" + name +"', CAPSULESPERBOX = " + capsulesPerBox + 
				   		", BOXPRICE = " + boxPrice + ", NUMBEROFCAPSULES = " + numberOfCapsules + ", NEWBOXPRICE = "+ newBoxPrice+ 
				   		", NEWNUMBEROFCAPSULES = "+ newNumberOfCapsules + " WHERE ID = " + id;
		   System.out.println(sql);
		   stmt.executeUpdate(sql);
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         se.printStackTrace(); 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
   }
   
   public static boolean createRecharge( Recharge recharge) {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		   String data_s = sdf.format(recharge.getData());
		   String sql = "INSERT INTO Transaction (id, amount, data, type, employee_id, cashflag, beverage_id, quantity) "
		   		+ "VALUES( "+ recharge.getId() + ", " + recharge.getAmount() + ", '" 
				   + data_s + "', 'R', " + recharge.getEmployee() +", NULL, NULL, NULL)";
		   stmt.executeUpdate(sql);
		   System.out.println(sql);
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         se.printStackTrace(); 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
   }
   
   public static boolean createConsumption( Consumption consumption) {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		   String data_s = sdf.format(consumption.getData());
		   String sql = "INSERT INTO Transaction "
		   		+ "VALUES( "+ consumption.getId() + ", " + consumption.getAmount() + ", '" 
				   + data_s+ "', 'C', " + consumption.getEmployee() +", "+ 
		   			consumption.getCashFlag()+", " + consumption.getBeverageId() + ", NULL)";
		   stmt.executeUpdate(sql);
		   System.out.println(sql);
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         se.printStackTrace(); 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
   }
   
   public static  boolean createBoxPurchase( BoxPurchase boxPurchase) {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		   String data_s = sdf.format(boxPurchase.getData());
		   
		   String sql = "INSERT INTO Transaction "
		   		+ "VALUES( "+ boxPurchase.getId() + ", " + boxPurchase.getAmount() + ", '" 
				   + data_s + "', 'B', NULL, NULL,  " + boxPurchase.getBeverageId() +", " 
		   		+ boxPurchase.getQuantity() + ")";
		   stmt.executeUpdate(sql);
		   System.out.println(sql);
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         se.printStackTrace(); 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
   }
   
   public static boolean updateLaTazzaBalance (Integer balance) {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   String sql = "UPDATE LaTazza SET BALANCE  =" + balance +" WHERE ID = 0";
		   System.out.println(sql);
		   stmt.executeUpdate(sql);
		   stmt.close(); 
	       conn.close(); 
	       return true;
	      } catch(SQLException se) { 
	       	         se.printStackTrace(); 
	       	         return false;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return false;
	      }
   }
   
  
   
   /*	METHODS USED AT APPLICATION BOOT	*/
   public static Map<Integer, Employee> getAllEmployees() {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   String sql = "SELECT * FROM EMPLOYEE";
		   System.out.println(sql);
		   ResultSet rs = stmt.executeQuery(sql); 
		   Map<Integer, Employee> map = new HashMap<Integer, Employee>();
		   Employee e;
		   while(rs.next()) { 
		          // Retrieve by column name 
			   	  
		          int id  = rs.getInt("id"); 
		          String name = rs.getString("name"); 
		          String surname = rs.getString("surname");
		          int balance = rs.getInt("balance"); 
		          e = new Employee(name, surname, id);
		          e.setBalance(balance);
		          map.put(id, e);
		       } 
		   rs.close(); 
		   stmt.close(); 
	       conn.close(); 
	       return map;
	      } catch(SQLException se) { 
	       	         System.out.println("Create table Employee ");
	       	         Database.createEmployeeTable();
	       	         return new HashMap<Integer, Employee>();
	      } catch(Exception ex) { 
	       	         ex.printStackTrace(); 
	       	         return null;
	      }
   }
   
   public static Map<Integer, Beverage> getAllBeverages() {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   String sql = "SELECT * FROM BEVERAGE";
		   System.out.println(sql);
		   ResultSet rs = stmt.executeQuery(sql); 
		   Map<Integer, Beverage> map = new HashMap<Integer, Beverage>();
		   Beverage b;
		   while(rs.next()) { 
		          // Retrieve by column name 
		          int id  = rs.getInt("id"); 
		          String name = rs.getString("name"); 
		          int capsulesPerBox  = rs.getInt("capsulesPerBox"); 
		          int boxPrice  = rs.getInt("boxPrice"); 
		          int numberOfCapsules  = rs.getInt("numberOfCapsules"); 
		          int newBoxPrice = rs.getInt("newBoxPrice");
		          int newNumberOfCapsules = rs.getInt("newNumberOfCapsules");
		          b = new Beverage(id, name, capsulesPerBox, boxPrice);
		          b.setNumberOfCapsules(numberOfCapsules);
		          b.setNewBoxPrice(newBoxPrice);
		          b.setNewNumberOfCapsules(newNumberOfCapsules);
		          map.put(id, b);
		       } 
		   rs.close(); 
		   stmt.close(); 
	       conn.close(); 
	       return map;
	      } catch(SQLException se) { 
	    	  		System.out.println("Create table Beverage ");
	    	  		Database.createBeverageTable();
	    	  		return new HashMap<Integer, Beverage>();
	      } catch(Exception ex) { 
	       	         ex.printStackTrace(); 
	       	         return null;
	      }
   }
   
   public static Map<Integer, Transaction> getAllTransactions() {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   String sql = "SELECT * FROM TRANSACTION";
		   System.out.println(sql);
		   ResultSet rs = stmt.executeQuery(sql); 
		   Map<Integer, Transaction> map = new HashMap<Integer, Transaction>();
		   Transaction t = null;
		   while(rs.next()) { 
		          // Retrieve by column name 
		          int id  = rs.getInt("id"); 
		          int amount  = rs.getInt("amount"); 
		          int quantity = 0;
		            
		          String data  = rs.getString("data"); 
		          
		          Date date1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(data);
		          String  type  = rs.getNString("type"); 
		          
		          switch (type.toUpperCase()) {
		          case "R":
		        	  int employeeId = rs.getInt("employee_id"); 
		        	  t = new Recharge(id, amount, employeeId);
		        	 break;
		          case "C":
		        	  int employee_Id = rs.getInt("employee_id"); 
		        	  boolean cashflag = rs.getBoolean("cashflag");
		        	  int beverage_Id = rs.getInt("beverage_id");
		        	  quantity = rs.getInt("quantity");
		        	  t = new Consumption(id, amount, quantity, cashflag, employee_Id, beverage_Id);
		        	  break;
		          case "B":
		        	  int beverageId = rs.getInt("beverage_id");
		        	  quantity = rs.getInt("quantity");
		        	  t = new BoxPurchase(id, amount, beverageId, quantity);
		        	  break;
		          default :
		        	  System.out.println("Type of Transaction INVALID: "+ type);
		        	  break;
		          }
		          
		          t.setData(date1);
		          map.put(id, t);
		          
		       } 
		   rs.close(); 
		   stmt.close(); 
	       conn.close(); 
	       return map;
	      } catch(SQLException se) { 
	    	  		System.out.println("Create table Transaction ");
	    	  		Database.createTransactionTable();
	    	  		return new HashMap<Integer, Transaction>();
	      } catch(Exception ex) { 
	       	         ex.printStackTrace(); 
	       	         return null;
	      }
   }
   
   public static int getLaTazzaBalance () {
	   Statement stmt = null;
	   
	   try {
		   Connection conn = connect();
		   stmt = conn.createStatement();
		   String sql = "SELECT balance FROM LaTazza WHERE ID = 0";
		   System.out.println(sql);
		   ResultSet rs = stmt.executeQuery(sql); 
		   int balance = 0;
		   while(rs.next()) { 
		          // Retrieve by column name 
			   	  
			   balance = rs.getInt("balance"); 
		       } 
		   rs.close(); 
		   stmt.close(); 
	       conn.close(); 
	       return balance;
	      } catch(SQLException se) { 
	       	         Database.createLaTazzaTable();
	       	         return 0;
	      } catch(Exception e) { 
	       	         e.printStackTrace(); 
	       	         return 0;
	      }
   }
   
   
}
