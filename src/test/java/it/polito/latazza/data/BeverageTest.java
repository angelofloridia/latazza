package it.polito.latazza.data;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.polito.latazza.data.database.Database;

class BeverageTest {

	@BeforeEach
	void setUp() throws Exception {
		Database.resetDatabase();
		LaTazza.reset();
		Database.firstConnect();
	}
	
	@Test
	void test() {
		Beverage bev = new Beverage(1, "arabica", 50, 130);
		bev.setBoxPrice(135);
		bev.setCapsulesPerBox(52);
		assertEquals((Integer)1, bev.getId());
		assertEquals("arabica", bev.getName());
		assertEquals((Integer)52, bev.getCapsulesPerBox());		
		assertEquals((Integer)135, bev.getBoxPrice());
		assertEquals((Integer)0, bev.getNumberOfCapsules());
		
		bev.setName("pincopallino");
		assertEquals("pincopallino", bev.getName());
		
		bev.setCapsulePrice(80);
		assertEquals((Integer)80, bev.getCapsulePrice());		
	}
}
