package it.polito.latazza.data;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.polito.latazza.data.database.Database;

public class BoxPurchaseTest {

	@BeforeEach
	void setUp() throws Exception {
		
		Database.resetDatabase();
		LaTazza.reset();
		Database.firstConnect();

	}
	
	@Test
	public void testGetSet() {
		Integer qt=3,idb=1,new_qt=8;
		BoxPurchase b=new BoxPurchase(1, 50, 1, 3);
		assertEquals(b.getBeverageId(),idb);
		assertEquals(b.getQuantity(),qt);
		b.setQuantity(8);
		assertEquals(b.getQuantity(),new_qt);
		
	}

}