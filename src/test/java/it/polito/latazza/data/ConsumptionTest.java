package it.polito.latazza.data;

import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.polito.latazza.data.database.Database;
import it.polito.latazza.exceptions.BeverageException;
import it.polito.latazza.exceptions.EmployeeException;

public class ConsumptionTest {
	Consumption c1;
	
	@BeforeEach
	void setUp() throws Exception {	
		Database.resetDatabase();
		LaTazza.reset();
		Database.firstConnect();

	}
	
	@Test
	public void testGetSet() {
		Integer ide=1,idb=1,new_idb=3,new_ide=3;
		Boolean cashflag=true,new_cashflag=false;
		c1=new Consumption(1, 50, 50, true, 1, 1);
		assertEquals(c1.getBeverageId(), idb);
		assertEquals(c1.getEmployee(), ide);
		assertEquals(c1.getCashFlag(), cashflag);
		c1.setBeverageId(new_idb);
		c1.setCashFlag(new_cashflag);
		c1.setEmployee(new_ide);
		assertEquals(c1.getBeverageId(), new_idb);
		assertEquals(c1.getEmployee(), new_ide);
		assertEquals(c1.getCashFlag(), new_cashflag);
	}
	@Test
	public void testToString() {
		Integer ide=0;
		Integer idb=0;
		try {
			ide = LaTazza.getInstance().getEmployeeManager().createEmployee("Matteo", "Borghesi"); 
			idb = LaTazza.getInstance().getInventoryManager().createBeverage("arabica", 30, 60);
		} catch (BeverageException | EmployeeException e) {
			fail();
		}
		c1=new Consumption(0, 2, 1, true, ide, idb);
		String testString = new String();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		testString += sdf.format(c1.getData()) + " ";
		testString += "CASH Matteo Borghesi arabica 1";
		assertEquals(c1.toString(), testString);
		
		Consumption c2 = new Consumption(0, 2, 1, true, null, idb);
		testString = sdf.format(c2.getData()) + " ";
		testString += "VISITOR arabica 1";
		assertEquals(c2.toString(), testString);
		
		Consumption c3 = new Consumption(0, 2, 1, false, ide, idb);
		testString = sdf.format(c3.getData()) + " ";
		testString += "BALANCE Matteo Borghesi arabica 1";
		assertEquals(c3.toString(), testString);
	}

}

