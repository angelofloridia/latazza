package it.polito.latazza.data;

import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.polito.latazza.data.database.Database;


public class DataImplTest {

	DataImpl dataImpl;
	Integer emplId, bevId, n;
		
	@BeforeEach
	void setUp() {
		Database.resetDatabase();
		LaTazza.reset();
		Database.firstConnect();

		dataImpl = new DataImpl();
		try {
			emplId = dataImpl.createEmployee("Matteo", "Borghesi");
			bevId = dataImpl.createBeverage("tea", 25, 2500); // 1€ per capsule
			LaTazza.getInstance().setBalance(10000); // 100€
			dataImpl.buyBoxes(bevId, 1); // Balance = 7500
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
		
	@Test
	public void testSellCapsules() {	
		try {
			LaTazza.getInstance().getEmployeeManager().getEmployeeById(emplId).setBalance(100);
			dataImpl.sellCapsules(emplId, bevId, 1, true);
			
			
			n = 0;
			assertEquals(dataImpl.getEmployeeBalance(emplId), n);
			n = 24;
			assertEquals(dataImpl.getBeverageCapsules(bevId), n);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}	
		
		try {
			dataImpl.sellCapsules(emplId, bevId, 1, true);
	
			n = -100;
			assertEquals(dataImpl.getEmployeeBalance(emplId), n);
			n = 23;
			assertEquals(dataImpl.getBeverageCapsules(bevId), n);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}			
		
	}
	
	@Test
	public void testSellCapsulesToVisitor() {
		try {
			dataImpl.sellCapsulesToVisitor(bevId, 2);
			n = 7500+200; // 2 capsules
			assertEquals(dataImpl.getBalance(), n);
			n = 23;
			assertEquals(dataImpl.getBeverageCapsules(bevId), n);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testRechargeAccount() {
		try {
			dataImpl.rechargeAccount(emplId, 100);
			n = 100;
			assertEquals(dataImpl.getEmployeeBalance(emplId), n);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testBuyBoxes() {
		try {
			dataImpl.buyBoxes(bevId, 2);
			n = 75;
			assertEquals(dataImpl.getBeverageCapsules(bevId), n);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testGetEmployeeReport() {
		try {
			Date d1 = new Date();
			dataImpl.sellCapsules(emplId, bevId, 2, false);
			Date d2 = new Date();
			ReportManager repMan = LaTazza.getInstance().getReportManager();
			assertEquals(dataImpl.getEmployeeReport(emplId, d1, d2), repMan.getEmployeeReport(emplId, d1, d2));
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testGetReport() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		Date d1 = null;
		try {
			d1 = sdf.parse( "31-08-2018 00:00:00");
			Date d2 = new Date();
			ReportManager repMan = LaTazza.getInstance().getReportManager();
			assertEquals(dataImpl.getReport(d1, d2), repMan.getReport(d1, d2));
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}		
	}
	
	
	@Test
	public void testCreateBeverage() {
		try {
			Integer coffeeId = dataImpl.createBeverage("coffee", 10, 1500);
			assertEquals(dataImpl.getBeverageName(coffeeId), "coffee");
		} catch (Exception e ) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testUpdateBeverage() {
		try {
			dataImpl.updateBeverage(bevId, "lemontea", 20, 40000);
			assertEquals(dataImpl.getBeverageName(bevId), "lemontea");
			n = 20;
			assertEquals(dataImpl.getBeverageCapsulesPerBox(bevId), n);
			n = 40000;
			assertEquals(dataImpl.getBeverageBoxPrice(bevId), n);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testGetBeveragesId() {
		try {
			Integer coffeeId = dataImpl.createBeverage("coffe", 10, 1500);
			ArrayList<Integer> testList = new ArrayList<Integer>();
			testList.add(bevId);
			testList.add(coffeeId);
			assertEquals(dataImpl.getBeveragesId(), testList);
		} catch (Exception e ) {
			e.printStackTrace();
			fail();
		} 
	}
	
	
	@Test
	public void testGetBeverages() {
		HashMap<Integer, String> testMap = new HashMap<Integer, String>();
		testMap.put(bevId, "tea");
		assertEquals(dataImpl.getBeverages(), testMap);
	}
	
	@Test
	public void testGetBeverageCapsules() {
		try {
			n = 25;
			assertEquals(dataImpl.getBeverageCapsules(bevId), n);
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	public void testCreateEmployee() {
		try {
			Integer empl2 = dataImpl.createEmployee("Mattia", "Tarquinio");
			assertEquals(dataImpl.getEmployeeName(empl2), "Mattia");
			assertEquals(dataImpl.getEmployeeSurname(empl2), "Tarquinio");
			n = 0;
			assertEquals(dataImpl.getEmployeeBalance(empl2), n);
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	public void testUpdateEmployee() {
		try {
			dataImpl.updateEmployee(emplId, "Angelo", "Floridia");
			assertEquals(dataImpl.getEmployeeName(emplId), "Angelo");
			assertEquals(dataImpl.getEmployeeSurname(emplId), "Floridia");
		} catch (Exception e) {
			fail();
		}
		
	}
	
	@Test
	public void testGetEmployeesId() {
		ArrayList<Integer> testList = new ArrayList<Integer>();
		testList.add(emplId);
		assertEquals(dataImpl.getEmployeesId(), testList);
	}
	
	@Test
	public void testGetEmployees() {
		HashMap<Integer, String> testMap = new HashMap<Integer, String>();
		testMap.put(emplId, "Matteo Borghesi");
		assertEquals(dataImpl.getEmployees(), testMap);
	}
	
	@Test
	public void testGetBalance() {
		n = 7500;
		assertEquals(dataImpl.getBalance(), n);
	}
	
	@Test
	public void testReset() {
		dataImpl.reset();
		n = 0;
		ArrayList<Integer> emptyList = new ArrayList<Integer>();
		HashMap<Integer, String> emptyMap = new HashMap<Integer, String>();
		assertEquals(dataImpl.getBalance(), n);
		assertEquals(dataImpl.getBeveragesId(), emptyList);
		assertEquals(dataImpl.getEmployeesId(), emptyList);
		assertEquals(dataImpl.getBeverages(), emptyMap);
		assertEquals(dataImpl.getEmployees(), emptyMap);
	}

}
