package it.polito.latazza.data;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.polito.latazza.data.database.Database;

//import it.polito.latazza.exceptions.EmployeeException;

class EmployeeManagerTest {

	EmployeeManager emplMan;
	
	@BeforeEach
	void setUp() throws Exception {
		Database.resetDatabase();
		LaTazza.reset();
		Database.firstConnect();

		emplMan = new EmployeeManager();	
		emplMan.getEmployees().clear();
		
	}

	@Test
	void testCreateEmployee() {
				
		try {
			emplMan.createEmployee("Matteo", "Borghesi");
		} catch (Exception e){
			fail();
		}
		
		try {
			emplMan.createEmployee("Matteo", "Borghesi");
			fail();
		} catch (Exception e){
			assertTrue(true);
		}
		
		try {
			emplMan.createEmployee("matteo", "borghesi");
		} catch (Exception e) {
			fail();
		}
		
		try {
			emplMan.createEmployee("Matteo", "Rosselli");
		} catch (Exception e) {
			fail();
		}
		
		try {
			emplMan.createEmployee("Maria", "Borghesi");
		} catch (Exception e) {
			fail();
		}		
	}

	@Test
	void testUpdateEmployee() {
		int id = 0;
		try {
			id = emplMan.createEmployee("Matteo", "Borghesi");
		} catch (Exception e){
			fail();
		}
		
		
		try {
			emplMan.updateEmployee(id, "Mattia", "Tarquinio");
			Employee testEmpl = emplMan.getEmployeeById(id);
			assertEquals(testEmpl.getName(), "Mattia");
			assertEquals(testEmpl.getSurname(), "Tarquinio");
		} catch (Exception e) {
			fail();
		}
		
		try {
			emplMan.updateEmployee(id+1, "Mattia", "Tarquinio");
			fail();
		} catch (Exception e) {
			assertTrue(true);
		}
		
	}

	@Test
	void testGetEmployeeAttributes() {
		int id = 0;
		Integer n2=12;
		try {
			id = emplMan.createEmployee("Matteo", "Borghesi");
			emplMan.getEmployeeById(id).setBalance(12);			
			String name = emplMan.getEmployeeName(id);
			String surname = emplMan.getEmployeeSurname(id);
			Integer balance = emplMan.getEmployeeBalance(id);
			assertEquals(name, "Matteo");
			assertEquals(surname, "Borghesi");
			assertEquals(balance, n2);
		} catch (Exception e){
			fail();
		}		
	}

	@Test
	void testRecordConsumption() {
		try {
			Integer id = emplMan.createEmployee("Matteo", "Borghesi");
			Consumption cons = new Consumption(0, 0, 0, false, null, 0);
			emplMan.recordConsumption(id, cons);
			Employee empl = emplMan.getEmployeeById(id);
			assertTrue(empl.getConsumptions().contains(cons));
		} catch (Exception e) {
			fail();
		}
		 	
	}

	@Test
	void testRecordRecharge() {
		try {
			Integer id = emplMan.createEmployee("Matteo", "Borghesi");
			Recharge rech = new Recharge(0, 0, null);
			emplMan.recordRecharge(id, rech);
			Employee empl = emplMan.getEmployeeById(id);
			assertTrue(empl.getRecharges().contains(rech));
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	void testGetEmployeeById() {
		try {
			Employee empl = emplMan.getEmployeeById(null);
			assertSame(empl, null);	
		} catch(Exception e) {
			fail();
		}
		
		Integer id = 0;
		try {
			id = emplMan.createEmployee("Matteo", "Borghesi");
			Employee empl = emplMan.getEmployeeById(id);
			assertEquals(empl.getName(), "Matteo");
			assertEquals(empl.getSurname(), "Borghesi");
		} catch (Exception e) {
			fail();
		}
		
		try {
			emplMan.getEmployeeById(id+1);
		} catch (Exception e) {
			assertTrue(true);
		}
	}

	@Test
	void testGetEmployeesId() {
		/* la lista di employee non viene svuotata*/
		assertTrue(emplMan.getEmployeesId().isEmpty());
		
		try {
			Integer id1 = emplMan.createEmployee("Matteo", "Borghesi");
			Integer id2 = emplMan.createEmployee("Matteo", "Tarquinio");
			ArrayList<Integer> testList = new ArrayList<Integer>();
			testList.add(id1);
			testList.add(id2);
			assertEquals(emplMan.getEmployeesId(), testList);
		} catch (Exception e) {
			fail();
		}				
	}

	@Test
	void testGetEmployees() {
		assertTrue(emplMan.getEmployees().isEmpty());
		
		try {
			Integer id1 = emplMan.createEmployee("Matteo", "Borghesi");
			Integer id2 = emplMan.createEmployee("Mattia", "Tarquinio");
			HashMap<Integer, String> testMap = new HashMap<Integer, String>();
			testMap.put(id1, "Matteo Borghesi");
			testMap.put(id2, "Mattia Tarquinio");
			assertEquals(emplMan.getEmployees(), testMap);
		} catch (Exception e) {
			fail();
		}
	}

}
