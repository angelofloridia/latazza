package it.polito.latazza.data;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.polito.latazza.data.database.Database;

import java.util.HashSet;

class EmployeeTest {

	Employee empl;
	
	@BeforeEach
	void setUp() throws Exception {
		empl = new Employee("Matteo", "Borghesi", 1);
		Database.resetDatabase();
		LaTazza.reset();
		Database.firstConnect();

	}
	
	@Test
	void testGetterSetter() {
		Integer n1=1,n2=0,n3=10;
		assertEquals(empl.getName(), "Matteo");
		assertEquals(empl.getSurname(), "Borghesi");
		assertEquals(empl.getId(), n1);
		assertEquals(empl.getBalance(), n2);
		
		empl.setName("Mattia");
		assertEquals(empl.getName(), "Mattia");
		empl.setSurname("Tarquinio");
		assertEquals(empl.getSurname(), "Tarquinio");
		empl.setBalance(10);	
		assertEquals(empl.getBalance(), n3);
	}
	
	
	
	@Test
	void testConsumptions() {				
		
		HashSet<Consumption> testSet = new HashSet<Consumption>();
		assertEquals(empl.getConsumptions(), testSet);
		
		empl.recordConsumption(null);
		assertEquals(empl.getConsumptions(), testSet);
		Consumption cons1 = new Consumption(0, 0, 0, false, null, 0);
		testSet.add(cons1);
		empl.recordConsumption(cons1);
		assertEquals(empl.getConsumptions(), testSet);
		empl.recordConsumption(null);
		assertEquals(empl.getConsumptions(), testSet);
		Consumption cons2 = new Consumption(0, 0, 0, false, null, 0);
		testSet.add(cons2);
		empl.recordConsumption(cons2);
		assertEquals(empl.getConsumptions(), testSet);			
		testSet.add(cons2);
		empl.recordConsumption(cons2);
		assertEquals(empl.getConsumptions(), testSet);
		
	}

	@Test
	void testRecharges() {
		HashSet<Recharge> testSet = new HashSet<Recharge>();
		assertEquals(empl.getRecharges(), testSet);
		
		empl.recordRecharge(null);
		assertEquals(empl.getRecharges(), testSet);
		Recharge rech1 = new Recharge(0, 0, null);
		testSet.add(rech1);
		empl.recordRecharge(rech1);
		assertEquals(empl.getRecharges(), testSet);
		empl.recordRecharge(null);
		assertEquals(empl.getRecharges(), testSet);
		Recharge rech2 = new Recharge(0, 0, null);
		testSet.add(rech2);
		empl.recordRecharge(rech2);
		assertEquals(empl.getRecharges(), testSet);			
		testSet.add(rech2);
		empl.recordRecharge(rech2);
		assertEquals(empl.getRecharges(), testSet);
	}

	@Test
	void testToString() {
		Employee empl2 = new Employee("", "", 2);
		assertEquals(empl.toString(), "Matteo Borghesi");
		assertEquals(empl2.toString(), " ");
	}
}
