package it.polito.latazza.data;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.polito.latazza.data.database.Database;
import it.polito.latazza.exceptions.BeverageException;
import it.polito.latazza.exceptions.EmployeeException;
import it.polito.latazza.exceptions.NotEnoughBalance;
import it.polito.latazza.exceptions.NotEnoughCapsules;

public class InventoryManagerTest {
	InventoryManager inv;
	
	@BeforeEach
	void Setup() {
		Database.resetDatabase();
		LaTazza.reset();
		Database.firstConnect();

		inv=new InventoryManager();
		
	}
	
	@Test
	public void testCreateBeverage() {
		Integer ib=0,price=3,nbox=50;
		try {
			ib=inv.createBeverage("arabica", 50, 3);
		} catch (BeverageException e) {
			fail();
		}
				
		try {
			assertEquals(inv.getBeverageBoxPrice(ib), price);
		} catch (BeverageException e) {
			fail();
		}
		try {
			assertEquals(inv.getBeverageCapsulesPerBox(ib), nbox);
		} catch (BeverageException e) {
			fail();
		}
		try {
			assertEquals(inv.getBeverageName(ib), "arabica");
		} catch (BeverageException e) {
			fail();
		}
		

	}
	
	
	@Test
	public void testUpdateBeverage() {
		Integer ib=0,price=3,nbox=30;
		try {
			ib=inv.createBeverage("arabica", 50, 3);
		} catch (BeverageException e) {
			fail();
		}
		
		try {
			inv.updateBeverage(ib, "espresso", 30, 3);
			assertEquals(inv.getBeverageName(ib), "espresso");
			assertEquals(inv.getBeverageBoxPrice(ib), price);
			assertEquals(inv.getBeverageCapsulesPerBox(ib), nbox);
		} catch (BeverageException e) {
			fail();
		}
		
		try {
			inv.updateBeverage(ib+1, "oro", 20, 20);
			fail();
		} catch (BeverageException e) {
			assertTrue(true);
		}
		
		
	}
	
	@Test
	public void testGetBeverageName() {
		Integer ib=0;
		try {
			ib=inv.createBeverage("arabica", 50, 3);
			assertEquals(inv.getBeverageName(ib), "arabica");
		} catch (BeverageException e) {
			fail();
		}
		
		try {
			inv.getBeverageName(ib+1);
			fail();
		} catch (BeverageException e) {
			assertTrue(true);
		}
		
	}
	
	@Test
	public void testGetBeverageCapsulesPerBox() {
		Integer ibe=0,nbox=50;
		try {
			ibe=inv.createBeverage("arabica", 50, 3);
			assertEquals(inv.getBeverageCapsulesPerBox(ibe), nbox);
		} catch (BeverageException e) {
			fail();
		}
		
	}
	
	@Test
	public void testgetBeverageBoxPrice() {
		Integer ibe=0,price=3;
		try {
			ibe=inv.createBeverage("arabica", 50, 3);
			assertEquals(inv.getBeverageBoxPrice(ibe), price);
		} catch (BeverageException e) {
			fail();
		}
		
	}
	
	@Test
	public void testgetBeveragesId() {
		Integer ib1=0,ib2=0;
		
		try {
			ib1=inv.createBeverage("arabica", 50, 3);
			ib2=inv.createBeverage("espresso", 30, 2);
			ArrayList<Integer> list=new ArrayList<Integer>();
			list.add(ib1);
			list.add(ib2);
			assertEquals(inv.getBeveragesId(), list);
			
		} catch (BeverageException e) {
			fail();
		}
		
	}
	
	
	@Test
	public void testgetBeverages() {
		Integer ib1=0,ib2=0;
		
		try {
			ib1=inv.createBeverage("arabica", 50, 3);
			ib2=inv.createBeverage("espresso", 30, 2);
			HashMap<Integer,String> list=new HashMap<Integer,String>();
			list.put(ib1,"arabica");
			list.put(ib2,"espresso");
			assertEquals(inv.getBeverages(), list);
		} catch (BeverageException e) {
			fail();
		}
		
	}
	
	@Test
	public void testSetGetBeverageCapsules() {
		Integer ib=0,nbox=100;
		try {
			ib=inv.createBeverage("arabica", 50, 3);
			inv.setBeverageCapsules(ib, 100);
			assertEquals(inv.getBeverageCapsules(ib), nbox);
		} catch (BeverageException e) {
			fail();
		}
		
	}
	
	@Test
	public void testGetBeveragebyType() {
		Integer ib=0;
		Long n=(long) 1;
		String name="arabica";
		try {
			ib=inv.createBeverage("arabica", 50, 3);
			inv.setBeverageCapsules(ib, 300);
			assertEquals(inv.getBeverageByType(name), n);
		} catch (BeverageException e) {
			fail();
		}
		
		
	}
	
	@Test
	public void testGetCapsulePrice() {
		
		Integer ib=0,num=200;

		try {
			ib=inv.createBeverage("arabica", 50,3);
		} catch (BeverageException e) {
			e.printStackTrace();
			fail();
		}
		inv.getBeverageById(ib).setCapsulePrice(200);
		assertEquals(inv.getCapsulePrice(ib), num);
		
		
	}
	
}

