package it.polito.latazza.data;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.polito.latazza.data.database.Database;
import it.polito.latazza.exceptions.BeverageException;
import it.polito.latazza.exceptions.EmployeeException;
import it.polito.latazza.exceptions.NotEnoughBalance;
import it.polito.latazza.exceptions.NotEnoughCapsules;

class LaTazzaTest {

	LaTazza latazza;
	
	@BeforeEach
	void setUp() throws Exception {
		Database.resetDatabase();
		LaTazza.reset();
		Database.firstConnect();

		latazza = LaTazza.getInstance();		
	}

	
	@Test
	void testGetInstance() {
		assertSame(LaTazza.getInstance(), latazza);
	}

	@Test
	void testGetterSetter() {
		Integer n1=0,n2=9;
		assertEquals(latazza.getBalance(), n1);
		latazza.setBalance(9);
		assertEquals(latazza.getBalance(), n2);
	}

	@Test
	void testReset() {
		EmployeeManager emplMan = latazza.getEmployeeManager();
		TransactionManager transMan = latazza.getTransactionManager();
		InventoryManager invMan = latazza.getInventoryManager();
		ReportManager repMan = latazza.getReportManager();
				
					
		try {
			Integer emplId = emplMan.createEmployee("Matteo", "Borghesi");
			Integer bevId = invMan.createBeverage("Tea", 1, 0);
			transMan.buyBoxes(bevId, 2);
			transMan.sellCapsules(emplId, bevId, 1, false);
		} catch (EmployeeException | BeverageException | NotEnoughCapsules | NotEnoughBalance e) {		
			e.printStackTrace();
			fail();
		}
		
		
		
		latazza.setBalance(15);
		Integer n1=0;
		LaTazza.reset();
		assertNotSame(LaTazza.getInstance(), latazza);
		latazza = LaTazza.getInstance();
		assertNotSame(latazza.getEmployeeManager(), emplMan);
		assertNotSame(latazza.getTransactionManager(), transMan);
		assertNotSame(latazza.getInventoryManager(), invMan);
		assertNotSame(latazza.getReportManager(), repMan);
		//System.out.println(latazza.getBalance() + " " + n1);
		assertEquals(latazza.getBalance(), n1);
		assertTrue(latazza.getEmployeeManager().getEmployeesId().isEmpty());
		assertTrue(latazza.getInventoryManager().getBeveragesId().isEmpty());
		assertTrue(latazza.getTransactionManager().getTransactions().isEmpty());
	}

}
