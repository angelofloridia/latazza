package it.polito.latazza.data;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.polito.latazza.data.database.Database;
import it.polito.latazza.exceptions.EmployeeException;


public class NfrTest {

	DataImpl dataImpl;
	Integer emplId, bevId, n;
	Timestamp startTime, endTime;
		
	@BeforeEach
	void setUp() {
		Database.resetDatabase();
		LaTazza.reset();
		Database.firstConnect();

		dataImpl = new DataImpl();
		try {
			emplId = dataImpl.createEmployee("Matteo", "Borghesi");
			bevId = dataImpl.createBeverage("tea", 25, 2500); // 1€ per capsule
			LaTazza.getInstance().setBalance(10000); // 100€
			dataImpl.buyBoxes(bevId, 1); // Balance = 7500
			endTime = null;
			startTime = new Timestamp(System.currentTimeMillis());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
		
	@Test
	public void testSellCapsules_nfr() {		
		try {
			dataImpl.sellCapsules(emplId, bevId, 2, true);
			endTime = new Timestamp(System.currentTimeMillis()); 
			long msDelta = endTime.getTime() - startTime.getTime();
			assertTrue(msDelta < 500);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}			
	}
	
	@Test
	public void testSellCapsulesToVisitor_nfr() {
		try {
			dataImpl.sellCapsulesToVisitor(bevId, 2);
			endTime = new Timestamp(System.currentTimeMillis()); 
			long msDelta = endTime.getTime() - startTime.getTime();
			assertTrue(msDelta < 500);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testRechargeAccount_nfr() {
		try {
			dataImpl.rechargeAccount(emplId, 100);
			endTime = new Timestamp(System.currentTimeMillis()); 
			long msDelta = endTime.getTime() - startTime.getTime();
			assertTrue(msDelta < 500);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testBuyBoxes_nfr() {
		try {
			dataImpl.buyBoxes(bevId, 2);
			endTime = new Timestamp(System.currentTimeMillis()); 
			long msDelta = endTime.getTime() - startTime.getTime();
			assertTrue(msDelta < 500);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testGetEmployeeReport_nfr() {
		try {
			dataImpl.getEmployeeReport(emplId, new Date(), new Date());
			endTime = new Timestamp(System.currentTimeMillis()); 
			long msDelta = endTime.getTime() - startTime.getTime();
			assertTrue(msDelta < 500);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testGetReport_nfr() {
		try {
			dataImpl.getReport(new Date(), new Date());
			endTime = new Timestamp(System.currentTimeMillis()); 
			long msDelta = endTime.getTime() - startTime.getTime();
			assertTrue(msDelta < 500);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}		
	}
	
	
	@Test
	public void testCreateBeverage_nfr() {
		try {
			dataImpl.createBeverage("coffee", 10, 1500);
			endTime = new Timestamp(System.currentTimeMillis()); 
			long msDelta = endTime.getTime() - startTime.getTime();
			assertTrue(msDelta < 500);
		} catch (Exception e ) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testUpdateBeverage_nfr() {
		try {
			dataImpl.updateBeverage(bevId, "lemontea", 20, 40000);
			endTime = new Timestamp(System.currentTimeMillis()); 
			long msDelta = endTime.getTime() - startTime.getTime();
			assertTrue(msDelta < 500);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testGetBeveragesId_nfr() {
		try {
			dataImpl.getBeveragesId();
			endTime = new Timestamp(System.currentTimeMillis()); 
			long msDelta = endTime.getTime() - startTime.getTime();
			assertTrue(msDelta < 500);
		} catch (Exception e ) {
			e.printStackTrace();
			fail();
		} 
	}
	
	
	@Test
	public void testGetBeverages_nfr() {
		dataImpl.getBeverages();
		endTime = new Timestamp(System.currentTimeMillis()); 
		long msDelta = endTime.getTime() - startTime.getTime();
		assertTrue(msDelta < 500);
	}
	
	@Test
	public void testGetBeverageCapsules_nfr() {		
		try {
			dataImpl.getBeverageCapsules(bevId);
			endTime = new Timestamp(System.currentTimeMillis()); 
			long msDelta = endTime.getTime() - startTime.getTime();
			assertTrue(msDelta < 500);
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	public void testCreateEmployee_nfr() {
		try {
			dataImpl.createEmployee("Mattia", "Tarquinio");
			endTime = new Timestamp(System.currentTimeMillis()); 
			long msDelta = endTime.getTime() - startTime.getTime();
			assertTrue(msDelta < 500);
		} catch (Exception e) {
			fail();
		}
	}
	
	@Test
	public void testUpdateEmployee_nfr() {
		try {
			dataImpl.updateEmployee(emplId, "Angelo", "Floridia");
			endTime = new Timestamp(System.currentTimeMillis()); 
			long msDelta = endTime.getTime() - startTime.getTime();
			assertTrue(msDelta < 500);
		} catch (Exception e) {
			fail();
		}
		
	}
	
	@Test
	public void testGetEmployeesId_nfr() {
		dataImpl.getEmployeesId();
		endTime = new Timestamp(System.currentTimeMillis()); 
		long msDelta = endTime.getTime() - startTime.getTime();
		assertTrue(msDelta < 500);
	}
	
	@Test
	public void testGetEmployees_nfr() {		
		dataImpl.getEmployees();
		endTime = new Timestamp(System.currentTimeMillis()); 
		long msDelta = endTime.getTime() - startTime.getTime();
		assertTrue(msDelta < 500);
	}
	
	@Test
	public void testGetBalance_nfr() {		
		dataImpl.getBalance();
		endTime = new Timestamp(System.currentTimeMillis()); 
		long msDelta = endTime.getTime() - startTime.getTime();
		assertTrue(msDelta < 500);
	}
	
	@Test
	public void testReset_nfr() {
		dataImpl.reset();
		endTime = new Timestamp(System.currentTimeMillis()); 
		long msDelta = endTime.getTime() - startTime.getTime();
		assertTrue(msDelta < 500);
	}
	
	@Test
	public void testNfr5() {
		
		Locale.setDefault(Locale.GERMANY); // In Germany the decimal separator is ","
		
		try {
			Date d1=new Date();
			dataImpl.rechargeAccount(emplId, 100);
			Date d2 = new Date();			
			String rechargeStr[] = dataImpl.getEmployeeReport(emplId, d1, d2).get(0).split(" ");
			String amountStr = rechargeStr[rechargeStr.length-2];
			System.out.println(amountStr);
			assertEquals(amountStr, "1.00");			
		} catch (Exception e) {			
			fail();
		}				
		
	}
	
}
