package it.polito.latazza.data;

import static org.junit.jupiter.api.Assertions.*;

import java.text.SimpleDateFormat;
import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.polito.latazza.data.database.Database;
import it.polito.latazza.exceptions.EmployeeException;

public class RechargeTest {

	@BeforeEach
	void setUp() throws Exception {
		Database.resetDatabase();
		LaTazza.reset();
		Database.firstConnect();

	}
	
	@Test
	public void testGetSet() {
		Integer id=1,new_id=5;
		Recharge r=new Recharge(1, 100, 1);
		assertEquals(r.getEmployee(), id);
		r.setEmployee(5);
		assertEquals(r.getEmployee(), new_id);
	}
	
	
	@Test
	public void testtoString() {
		Integer emplId = 0;
		try {
			emplId = LaTazza.getInstance().getEmployeeManager().createEmployee("Matteo", "Borghesi");
		} catch (EmployeeException e) {
			e.printStackTrace();
			fail();
		}
		Recharge r=new Recharge(1, 100, emplId);
		String toReturn = new String();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		toReturn += sdf.format(r.getData()) + " ";
		toReturn += "RECHARGE ";
		try {
			toReturn += LaTazza.getInstance().getEmployeeManager().getEmployeeById(emplId).toString() + " ";
		} catch (EmployeeException e) {
			fail();
			e.printStackTrace();
		}
		double amount_d = (double) r.getAmount();
		toReturn += String.format(Locale.US,"%.2f \u20ac", amount_d/100);
		assertEquals(r.toString(), toReturn);
		//commento
		

	}

}
