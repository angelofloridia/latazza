package it.polito.latazza.data;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.polito.latazza.data.database.Database;
import it.polito.latazza.exceptions.BeverageException;
import it.polito.latazza.exceptions.DateException;
import it.polito.latazza.exceptions.EmployeeException;
import it.polito.latazza.exceptions.NotEnoughBalance;
import it.polito.latazza.exceptions.NotEnoughCapsules;

class ReportManagerTest {

	ReportManager repMan;
	
	@BeforeEach
	void setUp() throws Exception {
		Database.resetDatabase();
		LaTazza.reset();
		Database.firstConnect();

		repMan = new ReportManager();		
	}

	@Test
	void testGetReport() {
		Date d1 = new Date();
		ArrayList <String> testList = new ArrayList<String>(); 
		try {
			assertEquals(repMan.getReport(d1, d1), testList);
		} catch (Exception e) {
			fail();
		}
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e2) {
			fail();
		}
		Date d2 = new Date();
		try {
			repMan.getReport(d2, d1);
			fail();
		} catch (Exception e) {
			assertTrue(true);
		}
		
		try {
			assertEquals(repMan.getReport(d1, d2), testList);
		} catch (Exception e) {
			assertTrue(true);
		}		
		
		Integer bevId = 0;
		try {
			bevId = LaTazza.getInstance().getInventoryManager().createBeverage("tea", 1, 0);
		} catch (BeverageException e1) {
			fail();
		}
		try {
			LaTazza.getInstance().getTransactionManager().buyBoxes(bevId, 1);
		} catch (Exception e) {
			fail();			
		}
		
		Transaction b = LaTazza.getInstance().getTransactionManager().getTransactions().values().iterator().next();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");		
		String entry = sdf.format(b.getData()) + " BUY tea 1";
		testList.add(entry);
		try {			
			assertEquals(repMan.getReport(b.getData(), b.getData()), testList);			
		} catch (DateException e) {			
			fail();
		}
	}
	
	@Test
	void testGetEmployeeReport() {
							
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
		Date d1 = null;
		Date d2 = null;
		try {
			d1 = sdf.parse( "31-08-2018 00:00:00");
			d2 = sdf.parse( "31-08-2019 00:00:00");
		} catch (ParseException e1) {
			e1.printStackTrace();
			fail();
		}
		
		
		EmployeeManager emplMan = LaTazza.getInstance().getEmployeeManager();
		TransactionManager transMan = LaTazza.getInstance().getTransactionManager();
		InventoryManager invMan = LaTazza.getInstance().getInventoryManager();
		Integer emplId = 0;
		try {
			emplId = emplMan.createEmployee("Matteo", "Borghesi");
			LaTazza.getInstance().setBalance(100);
			Integer bevId = invMan.createBeverage("tea", 25, 100);
			transMan.buyBoxes(bevId, 1);			
			transMan.rechargeAccount(emplId, 100);
			transMan.sellCapsules(emplId, bevId, 0, true);		
			
			
			String cons_s = emplMan.getEmployeeById(emplId).getConsumptions().iterator().next().toString();
			String rech_s = emplMan.getEmployeeById(emplId).getRecharges().iterator().next().toString();			
			ArrayList<String> testList = new ArrayList<String>();
			testList.add(rech_s);	
			testList.add(cons_s);
				
			assertEquals(repMan.getEmployeeReport(emplId, d1, d2), testList);
		} catch (Exception e) {		
			e.printStackTrace();
			fail();
		}
		
		try {
			repMan.getEmployeeReport(emplId, d2, d1);			
			fail();
		} catch (Exception e) {
			assertTrue(true);
		}
	}

}
