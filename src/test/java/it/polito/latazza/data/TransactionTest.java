package it.polito.latazza.data;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.polito.latazza.data.database.Database;

public class TransactionTest {
	
	Transaction t;
	
	@BeforeEach
	void setUp() throws Exception {	
		Database.resetDatabase();
		LaTazza.reset();
		Database.firstConnect();

	}
	
	@Test
	public void testSetGet() {
		Integer id=1,amount=40,newamount=80;
		t=new Transaction(1, 40);
		Date d=new Date();
		assertEquals(t.getData(), d);
		assertEquals(t.getId(), id);
		assertEquals(t.getAmount(), amount);
		t.setAmount(80);
		assertEquals(t.getAmount(), newamount);
			SimpleDateFormat format =
		        new SimpleDateFormat("dd/mm/yyyy");
		    try {
				d = format.parse("10/05/2019");
				
			} catch (ParseException e) {
				fail();
			}	
		    t.setData(d);
		    assertEquals(t.getData(), d);

	}

}
