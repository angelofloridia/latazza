package it.polito.latazza.data;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import it.polito.latazza.data.database.Database;
import it.polito.latazza.exceptions.BeverageException;
import it.polito.latazza.exceptions.EmployeeException;
import it.polito.latazza.exceptions.NotEnoughBalance;
import it.polito.latazza.exceptions.NotEnoughCapsules;

class TransactionmanagerTest {
	
	TransactionManager tr;
	EmployeeManager empl;
	InventoryManager inv;
	
	
	@BeforeEach
	void Setup() {
		Database.resetDatabase();
		LaTazza.reset();
		Database.firstConnect();

		tr= LaTazza.getInstance().getTransactionManager();
		empl= LaTazza.getInstance().getEmployeeManager();
		inv= LaTazza.getInstance().getInventoryManager();
	}
	
	@Test
	void testsellCapsules() {
		Integer ide=0,idb=0,newamount=1002;
		
		try {
			ide=empl.createEmployee("claudio", "bianchi");
		} catch (EmployeeException e) {
			fail();
		}
		
		try {
			idb=inv.createBeverage("arabica", 50, 3);
			inv.getBeverageById(idb).setNumberOfCapsules(300);
			inv.getBeverageById(idb).setCapsulePrice(2);
			LaTazza.getInstance().setBalance(1000);
		} catch (BeverageException e) {
			fail();
		}
		
		try {
			tr.sellCapsules(ide, idb, 301, false);
			fail();
		} catch (NotEnoughCapsules e) {
			assertTrue(true);
		} catch (Exception e) {
			fail();
		}		
		
		try {
			tr.sellCapsules(ide, idb, 1, false);
			tr.sellCapsules(ide, idb, 0, true);
		} catch (EmployeeException | BeverageException | NotEnoughCapsules e) {
			fail();
		}
		Integer numbercapsules=299;
		Integer n=LaTazza.getInstance().getBalance();
		assertEquals(n, newamount);
		try {
			assertEquals(inv.getBeverageCapsules(idb),numbercapsules);
		} catch (BeverageException e) {
			fail();
		}
		
		try {
			tr.sellCapsules(null, 0, 0, false);
			fail();
		} catch (EmployeeException e) {
			assertTrue(true);
		} catch (Exception e) {
			fail();
		}

		try {
			tr.sellCapsules(ide, idb+1, 0, false);
			fail();
		} catch (Exception e) {
			assertTrue(true);
		}
		
		
		
	}
	
	
	@Test
	void testsellCapsulestoVisitor() {
		/*la lista locale di beverages non viene svuotata*/
		Integer newamount,idb=0;
		try {
			idb=inv.createBeverage("arabica", 50, 3);
			System.out.println("1");
			inv.getBeverageById(idb).setNumberOfCapsules(300);
			System.out.println("2");
			inv.getBeverageById(idb).setCapsulePrice(2);
			System.out.println("3");
		} catch (BeverageException e) {
			fail();
		}
		newamount=LaTazza.getInstance().getBalance()+4;
		try {
			tr.sellCapsulesToVisitor(idb, 2);
		} catch (BeverageException | NotEnoughCapsules e) {
			fail();
		}
		Integer n=LaTazza.getInstance().getBalance();
		assertEquals(n, newamount);
		
	}
	
	
	@Test
	void testrechargeAccount() {
		Integer ide=0,amount=500;
		try {
			ide=empl.createEmployee("giorgio", "rossi");
		} catch (EmployeeException e) {
			
			fail();
		}
		try {
			tr.rechargeAccount(ide, amount);
		} catch (EmployeeException e) {
			fail();
		}
		try {
			assertEquals(empl.getEmployeeBalance(ide), amount);
		} catch (EmployeeException e) {
			fail();
		}
	}
	
	
	@Test
	void testBuyBoxes() {
		Integer idb=0,newamount;
		LaTazza.getInstance().setBalance(1000);
		try {
			idb=inv.createBeverage("arabica", 50, 3);
		} catch (BeverageException e) {
			fail();
		}
		newamount=LaTazza.getInstance().getBalance()-3;
		try {
			tr.buyBoxes(idb, 1);
		} catch (BeverageException | NotEnoughBalance e) {
			fail();
		}
		assertEquals(LaTazza.getInstance().getBalance(), newamount);
		
		try {
			tr.buyBoxes(idb, 400);
			fail();
		} catch (NotEnoughBalance e) {
			assertTrue(true);
		} catch (Exception e) {
			fail();
		}
		
		
	}
}
