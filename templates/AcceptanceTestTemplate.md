# Acceptance Testing Documentation template

Authors: Matteo Borghesi, Angelo Floridia, Lorenzo Marzolla, Mattia Tarquinio

Date: 26/5/2019

Version: v1.1.0

# Contents

- [Scenarios](#scenarios)

- [Coverage of scenarios](#scenario-coverage)
- [Coverage of non-functional requirements](#nfr-coverage)



# Scenarios

```
<Define here additional scenarios for the application. The two original scenarios in the Requirements Document are reported here.>
```

| Scenario ID: SC1 | Corresponds to UC1                             |
| ---------------- | ---------------------------------------------- |
| Description      | Colleague uses one capsule of type T         |
| Precondition     | account of C has enough money to buy capsule T |
| Postcondition    | account of C updated, count of T updated       |
| Step#            | Step description                               |
| 1                | Administrator selects capsule type T           |
| 2                | Administrator selects colleague C              |
| 3                | Deduce one for quantity of capsule T           |
| 4                | Deduce price of T from account of C            |

| Scenario ID: SC2 | Corresponds to UC2                                     |
| ---------------- | ------------------------------------------------------ |
| Description      | Colleague uses one capsule of type T, account negative |
| Precondition     | account of C has not enough money to buy capsule T     |
| Postcondition    | account of C updated, count of T updated               |
| Step#            | Step description                                       |
| 1                | Administrator selects capsule type T                   |
| 2                | Administrator selects colleague C                      |
| 3                | Deduce one for quantity of capsule T                   |
| 4                | Deduce price of T from account of C                    |
| 5                | Account of C is negative, issue warning                |

| Scenario ID: SC3 | Corresponds to UC3 |
| ---------------- | ------------------ |
| Description      | Colleague recharges his/her account   |
| Precondition     | Colleague has an account with balance B  |
| Postcondition    | Colleague's account has balance B+X |
| Step#            | Step description |
| 1                | Administrator selects colleague C  |
| 2                 | Administrator inserts recharge amount X |
| 3		    | Add X to balance of Colleague |

| Scenario ID: SC4 | Corresponds to UC4 |
| ---------------- | ------------------ |
| Description      | Record purchase of  B boxes of type T  |
| Precondition     | Beverage T already exists and N capsules of it are available |
|				| T can be bought in boxes of M capsules each, each box costs P |
|				| The overall balance of the system is SB |
| Postcondition    | Capsules of T available are N + B*M |
|  			| Balance of the system is SB - B*P	|
| Step#            | Step description |
| 1                | Administrator selects beverage type T |
| 2                 | Administrator selects number of boxes B |
| 3		    | The number of available capsules is increased by B*M |
| 4		| The balance of the system is decreased by B*P |

| Scenario ID: SC5 | Corresponds to UC5 |
| ---------------- | ------------------ |
| Description      | Procuce report of colleague C |
| Precondition     | Colleague C exists |
| Postcondition    ||
| Step#            | Step description |
| 1                | Administrator selects colleague C |
| 2                 | Administrator selects start date D1 |
| 3		    | Administrator selects end date D2 |
| 4		| Report of all consumptions of C in the period is produced |


| Scenario ID: SC6 | Corresponds to UC6 |
| ---------------- | ------------------ |
| Description      | Procuce report of all consumptions |
| Precondition     |  |
| Postcondition    ||
| Step#            | Step description |
| 1                 | Administrator selects start date D1 |
| 2		    | Administrator selects end date D2 |
| 3		| Report of all consumptions in the period is produced |

| Scenario ID: SC7 | Corresponds to UC7 |
| ---------------- | ------------------ |
| Description      | Record usage of capsules by visitor |
| Precondition     | Beverage of type T exists and capsules are available |
| Postcondition    | Amount of the system is updated |
|     | Number of available capsules is updated |
| Step#            | Step description |
| 1                 | Administrator selects beverage type B |
| 2		    | Administrator selects number of capsules |
| 3		| Administrator selects visitor |
| 4		| Balance of the system is updated |
| 5		| Number of available capsules is updated |

# Coverage of Scenarios

```
<Report in the following table the coverage of the scenarios listed above. Report at least an API test (defined on the functions of DataImpl only) and a GUI test (created with EyeAutomate) for each of the scenarios. For each scenario, report the functional requirements it covers.
In the API Tests column, report the name of the method of the API Test JUnit class you created. In the GUI Test column, report the name of the .txt file with the test case you created.>
```

### 

FR7 and FR8 are too fuzzy and, as such, not testable.

| Scenario ID | Functional Requirements covered | API Test(s) | GUI Test(s) |
| ----------- | ------------------------------- | ----------- | ----------- |
| 1           | FR1                             |      DataImplTest.testSellCapsules       |   scenario1.txt          |
| 2           | FR1                             |     DataImplTest.testSellCapsules              | scenario2.txt            |
| 3         |        FR3                         |    DataImplTest.testRechargeAccount               | scenario3.txt            |
| 4         |     FR4                            |     DataImplTest.testBuyBoxes              |  scenario4.txt           |
| 5         |       FR5                          |      DataImplTest.testGetEmployeeReport             |  scenario5.txt           |
| 6         |         FR6                        |       DataImplTest.testGetReport      |  scenario6.txt           |
| 7         | FR2                                |      DataImplTest.testSellCapsulesToVisitor       | scenario7.txt            |



# Coverage of Non Functional Requirements

```
<Report in the following table the coverage of the Non Functional Requirements of the application - only those that can be tested with automated testing frameworks.>
```

### 

| Non Functional Requirement | Test name |
| -------------------------- | --------- |
| NFR2                          |       NfrTest.*     |
| NFR5			| NfrTest.testNfr5 |

