# Design Document Template

Authors: Matteo Borghetti, Angelo Floridia, Lorenzo Marzolla, Mattia Tarquinio

Date: 23/04/2019

Version: 1.0

# Contents

- [Package diagram](#package-diagram)
- [Class diagram](#class-diagram)
- [Verification traceability matrix](#verification-traceability-matrix)
- [Verification sequence diagrams](#verification-sequence-diagrams)

# Instructions

The design document has to comply with:
1. [Official Requirement Document](../Official\ Requirements\ Document.md)
2. [DataInterface.java](../src/main/java/it/polito/latazza/data/DataInterface.java)

UML diagrams **MUST** be written using plantuml notation.

# Package diagram

We are talking about a very simple system, a standalone application that 
must run on a single computer. If we wanted to link it to an architectural 
pattern, it could be a 3 tiers architecture.

```plantuml
package "Data" #DDDDDD {}
package "Exceptions" #DDDDDD {}
package "GUI" #DDDDDD {}

GUI --> Data
Data --> Exceptions
```


# Class diagram


The only package we're going to program is the  "Data" one, which contains all the business logic.
The word "persistent" has not been written on the individual classes, as it has been said that 
the entire state of the application must be able to be saved and recovered, so as to allow the 
application not to depend totally on the administrator. These operations will be performed 
on a small local database.

```plantuml

class LaTazza << (S, #FF7700) Singleton >>{
    - balance
    - LaTazza()
    + GetInstance()
    + GetInventoryManager()
    + GetEmployeeManager()
    + GetReportManager()
    + GetBalance()
    + SetBalance()
    + Reset()
}

class EmployeeManager{
    + EmployeeManager()
    + CreateEmplyee()
    + UpdateEmployee()
    + GetEmployeeName()
    + GetEmployeeSurname()
    + GetEmployeeBalance()
    + RecordConsumption()
    + RecordRecharge()
    + GetEmployeeById()
    + GetEmployeesId()
    + GetEmployees()
}

class InventoryManager{
    + CreateBeverage()
    + UpdateBeverage()
    + GetBeverageName()
    + GetBeverageCapsulePerBox()
    + GetBeverageBoxPrice()
    + GetBeveragesId()
    + GetBeverages()
    + GetBeverageCapsules()
    + SetBeverageCapsules()
    + GetBeverageById()
}

class TransactionManager{
    + SellCapsules()
    + SellCapsulesToVisitor()
    + RechargeAccount()
    + BuyBoxes()
    + GetTransactions()
}

class ReportManager{
    + GetEmployeeReport()
    + GetReport()
}

class Employee{
    - id
    - name
    - surname
    - balance
    + Employee()
    + GetName()
    + SetName()
    + GetSurname()
    + SetSurname()
    + GetBalance()
    + SetBalance()
    + GetId()
    + RecordConsumption()
    + RecordRecharge()
    + getConsumptions()
    + getRecharges()
    + toString()
}

class Beverage{
    - id
    - name
    - boxPrice
    - capsulePerBox
    - numberOfCapsules
    + Beverage()
    + GetId()
    + GetName()
    + SetName()
    + GetBoxPrice()
    + SetBoxPrice()
    + SetCapsulePerBox()
    + GetCapsulePerBox()
    + GetCapsulePrice()
    + SetCapsulePrice()
}


class Transaction{
    - id
    - date
    - amount
    + GetId()
    + GetDate()
    + SetDate()
    + GetAmount()
    + SetAmount()
}

class Recharge{
}

class Consumption{
    - cashFlag
    + GetCashFlag()
    + SetCashFlag()
}

class BoxPurchase{
    - quantity
    + GetQuantity()
    + SetQuantity()
}

class DataInterface<< (I, orchid)>> {
     + CreateEmplyee()
    + UpdateEmployee()
    + GetEmployeeName()
    + GetEmployeeSurname()
    + GetEmployeeBalance()
    + GetEmployeesId()
    + GetEmployees()
    + CreateBeverage()
    + UpdateBeverage()
    + GetBeverageName()
    + GetBeverageCapsulePerBox()
    + GetBeverageBoxPrice()
    + GetBeveragesId()
    + GetBeverages()
    + GetBeverageCapsules()
    + SellCapsules()
    + SellCapsulesToVisitor()
    + RechargeAccount()
    + BuyBoxes()
    + GetEmployeeReport()
    + GetReport()
    + GetBalance()
    + Reset()
}

class DataImpl{
}

LaTazza --> "1" EmployeeManager
LaTazza --> "1" InventoryManager
DataInterface --> "1" EmployeeManager
DataInterface --> "1" InventoryManager
DataInterface --> "1" TransactionManager
DataInterface --> "1" ReportManager
DataImpl ..|> DataInterface
EmployeeManager --> "'0..*" Employee
Employee --> "0..*" Recharge
Employee --> "0..*" Consumption
Recharge --> "1" Employee
Consumption --> "1" Employee
BoxPurchase --> "1" Beverage
BoxPurchase --|> Transaction
Consumption --|> Transaction
Recharge --|> Transaction
InventoryManager --> "0..*" Beverage
InventoryManager --> "0..*" Beverage
Consumption --> "1..*" Beverage
TransactionManager --> "0..*" Transaction

```

# Verification traceability matrix


|  | LaTazza | DataInterface  |  EmployeeManager |  TrasanctionManager |   ReportManager |  Employee |  InventoryManager |  BoxPurchase |  Recharge|  Consumption | Transaction|  Beverage | 
| ------------- |:-------------:| :-----:| :-----:| :-----:| :-----:| :-----:| :-----:| :-----:| :-----:| :-----:| :-----:| :-----:|
| FR1 | X | X | X |X | |X | | | |X |X |X |
| FR2  |X  | X | X|X | | | | | | |X |X | 
| FR3| X |X  | X|X | | X| | | X| |X | | | 
| FR4 | X | X |X |X | | |X | X| | | X| X| 
| FR5 |X | X |X | | X| X| | | |X |X | | 
| FR6 |  | X | | | X| | | | | X| X| | 
| FR7 | X |X | | | | |X | | | | | X| 
| FR8 | X |  X|X | | | X| | | | | | |

# Verification sequence diagrams 

```plantuml
DataImpl -> LaTazza: getInstance
LaTazza --> DataImpl

DataImpl -> LaTazza: getTransactionManager
LaTazza --> DataImpl

DataImpl -> TransactionManager : sellCapsules
activate TransactionManager

TransactionManager -> LaTazza : getEmployeeManager
LaTazza --> TransactionManager

TransactionManager -> EmployeeManager : getEmployees
EmployeeManager --> TransactionManager

TransactionManager -> Employee : getBalance
Employee --> TransactionManager

TransactionManager -> LaTazza : getInventoryManager
LaTazza --> TransactionManager

TransactionManager -> InventoryManager : getBeverages
InventoryManager --> TransactionManager

TransactionManager -> Beverage : getCapsulePrice
Beverage --> TransactionManager

note over TransactionManager: compare price with balance

TransactionManager -> Employee : setBalance

note over TransactionManager
    update available quantity:
    this is done internally
    in one of the data structures
    related with Beverage
end note

TransactionManager -> EmployeeManager : recordConsumption

TransactionManager --> DataImpl
deactivate TransactionManager
```