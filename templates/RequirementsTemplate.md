# Requirements Document Template

Authors: Matteo Borghesi, Angelo Floridia, Lorenzo Marzolla, Mattia Tarquinio

Date:   09/04/2019

Version: 0.2

# Contents

- [Stakeholders](#stakeholders)
- [Context Diagram and interfaces](#context-diagram-and-interfaces)
	+ [Context Diagram](#context-diagram)
	+ [Interfaces](#interfaces) 
	
- [Stories and personas](#stories-and-personas)
- [Functional and non functional requirements](#functional-and-non-functional-requirements)
	+ [Functional Requirements](#functional-requirements)
	+ [Non functional requirements](#non-functional-requirements)
- [Use case diagram and use cases](#use-case-diagram-and-use-cases)
	+ [Use case diagram](#use-case-diagram)
	+ [Use cases](#use-cases)
	+ [Relevant scenarios](#relevant-scenarios)
- [Glossary](#glossary)
- [System design](#system-design)

# Stakeholders


| Stakeholder name  | Description | 
| ----------------- |:-----------:|
| Employee          | Person who works in the work environment, he buy capsules using cash or local account | 
| Visitors          | Person who does not work in the environment but can buy capsules using cash only |
| Manager           | He is an employee. He has access to warehouse, inventory, orders, credit management of the employees and cash account using LaTazza|
| Company           | For who employees and manager are working for | 
| Supplier          | Supply coffee capsules in boxes of 50 capsules of the same kind |


# Context Diagram and interfaces

## Context Diagram

```plantuml
left to right direction
skinparam packageStyle rectangle

actor Manager as man
actor EmailSystem as es
actor PaymentSystem as pm
actor AccountSystem as as


rectangle System {
  (LaTazza) as t
   man -- t: "Manage"
   t -- es
   pm -- t
   t -- as
}

note  "The capsules supplier interact just with the Email System" as n
```

## Interfaces
| Actor | Logical Interface | Physical Interface  |
| ------------- |:-------------:| -----:|
| Manager      | GUI  | PC, internet connection |
| Payment System | Web services | Internet connection |
| Mail System | SMTP, POP, etc.. | Internet connection |
| Account System | JSON, XML, etc.. | Internet connection |

# Stories and personas

<img src="/templates/images_Stefano Bellomo.png">

Mr. Bellomo is responsible in his company for managing capsules to be used in the coffee maker.
Currently, he uses a spreadsheet to take note of how many capsules of each type are available. When he notices the capsules are running out, he emits an order to the supplier. The capsules are hold in the bottom drawer of his desk.
Mr. Bellomo's colleagues can buy capsules from him, but only cash payment is allowed, since the existing account system of the company doesn't have any feature for recording any pending payment. 
Therefore, in order to make his job easier Mr. Bellomo doesn't allow for credits or debts, i.e. his colleagues always receive the capsules they pay for at the moment.

Here is an example of Mr. Bellomo's interaction with the application:
*After lunch, some colleagues wish to have a coffee and turn to Mr. Bellomo to buy some capsules. Since he  doesn't want to spend too much time on that, he keep tracks on a slip of paper how many and what kind of capsules everyone wants and takes them from his office.
After lunch, Mr. Bellomo accesses the LaTazza application and records the informations he has written down. Then he takes a short look at the summary and checks if any capsules are running out. If this is the case, he emits quickly a supply order and gets back to his job.*

# Functional and non functional requirements

## Functional Requirements

| ID		| Description  |
| ---------- |:-------------:| 
| FR1	| Sell capsule | 
| FR2	| Pay capsule |
| FR2.1	| Pay cash |
| FR2.2	| Pay through account |
| FR3	| Buy capsules from supplier |
| FR4	| Load money into account |
| FR5	| Retrieve employee's balance |
| FR6	| Retrieve inventory status |
| FR7	| Retrieve list of pending supply orders |
| FR7.1	| Remove pending supply order from the list when received |
| FR7.2	| Add supply order to inventory when received |
| FR8	| Send payment order to employees with negative balance at the end of each month |
| FR9	| Block sell for employees with pending payment orders |
| FR10	| Set type and prices of capsules from configuration file |

## Non Functional Requirements

| ID        | Type (efficiency, reliability, ..)           | Description  | Refers to |
| ------------- |:-------------:| :-----:| -----:|
| NFR1	| Domain		| Payments are made with euros 	| FR1-5,10  |
| NFR2     | Usability	| Emitting an order should require 5 clicks or less  | FR3 |
| NFR3	| Usability	| Manager should be able to use the application after < 5 minutes training | FR1-7|
| NFR4	| Portability | Application should run on Linux systems | FR1-10 |
| NFR5	| Performance	| Response time to user inputs should be < 0.5s | FR1-10 |
| NFR6	| Legislative	| Orders to the supplier should be compliant with the italian fiscal rules | FR3 |


# Use case diagram and use cases

## Use case diagram
``` plantuml
left to right direction
skinparam packageStyle rectangle

actor Manager as m
actor AccountSystem as a
actor PaymentSystem as p
actor EMailSystem as s

rectangle system{
(handle sales  FR1,FR2) as bc
(account payment FR2.2) as aa
(cash payment FR2.1) as bb
(handle inventory FR6) as bs
(buy capsule FR3) as bss
(manage employees account FR4,FR5) as ma
}

m --> bs
m --> ma
bc <-- m
bc .> aa : include
bc .> bb : include
a <--aa
a <--ma
bs-->bss
p <-- bss

s <--bss

```

## Use Cases
<describe here each use case in the UCD>

### Handle sales (account payment), UC1
| Actors Involved        |Manager, AccountSystem |
| ------------- |:-------------:| 
|  Precondition     | Employee has a personal account |  
|  Post condition     | Capsule numbers in inventory and Employee balance are updated |
|  Nominal Scenario     | The manager access on LaTazza and select type and quantity, then select employee's account and proceed with payement(the request for a capsule is external to the system). If employee is in debt LaTazza blocks operation|
|  Variants     | There isn't type or quantity selected,employee account is blocked for debts |

### Handle sales (cash payment), UC2
| Actors Involved        |Manager |
| ------------- |:-------------:| 
|  Precondition     | Manager has recived money |  
|  Post condition     | Capsule numbers in inventory is updated |
|  Nominal Scenario     | Manager has already taken money so he proceed to sell capsule selecting quantity and type |
|  Variants     |There isn't type or quantity selected |
### Handle inventory (buy capsules), UC3
Actors Involved        |Manager, PaymentSystem, E-MailSystem|
| ------------- |:-------------:| 
|  Precondition     | Manager is logged with his credential |  
|  Post condition     | Order is saved into pendent-order list |
|  Nominal Scenario     | Manager select type and quantity of capsules for the order than procede to the payement: order si saved in the order list and a e-mail is send to supplier |
|  Variants     | Error with payment |
### Handle inventory (confirm order), UC4
Actors Involved        |Manager,|
| ------------- |:-------------:| 
|  Precondition     | Order is a pendent order |  
|  Post condition     | Inventory is updated and order is no more in pendent-order list |
|  Nominal Scenario     |When arrive an order Manager select it from the pendant order and confirm his delivery. System update inventory |
|  Variants     | / |
### Manage employee account, UC5
Actors Involved        |Manager, AccountSystem |
| ------------- |:-------------:| 
|  Precondition     | Manager is logged with his credential |  
|  Post condition     | Amount in employee account is updated |
|  Nominal Scenario     | Manager receives money from employee that wants recharge his account, then access on LaTazza and records the recharge  |
|  Variants     | Employee decide to use rest of his purchease to recharge account|

# Relevant scenarios


## Scenario 1

| Scenario ID: SC1        | Corresponds to UC:Handle sales (account payment) |
| ------------- |:-------------:| 
| Step#        | Description  |
|  1     |Manager log into LaTazza(has already recived the request)  |  
|  2     |Select beverage's type |
|  3     |Select quantity |
|  4     |Select Employee  |
|  5     |Select account in payment type|
|  6     |Procede with payment|
|  7     |Money is taken from employee's account |

## Scenario 2

| Scenario ID: SC2        | Corresponds to UC:Handle sales (account payment: account is blocked for debt) |
| ------------- |:-------------:| 
|  1     |Manager log into LaTazza(has already recived the request)  |  
|  2     |Select beverage's type |
|  3     |Select quantity |
|  4     |Select Employee  |
|  5     |Select account in payment type|
|  6     |Procede with payment|
|  7     |System show that operation isn't possible|
|  8     |System abort the operation|
|  9     |System return on main page|

## Scenario 3

| Scenario ID: SC3        | Corresponds to UC:Handle sales (cash payment) |
| ------------- |:-------------:| 
|  1     |Manager log into LaTazza(has already recived the request and money)  |
|  2     |Select beverage's type |
|  3     |Select quantity |
|  5     |Select cash in payment type|
|  6     |Procede with payment|
|  7     |Print the receipt(if requested)|

## Scenario 4

| Scenario ID: SC4        | Corresponds to UC:Handle sales (cash payment:there isn't type selected) |
| ------------- |:-------------:| 
|  1     |Manager log into LaTazza(has already recived the request and money)  |
|  2     |Select beverage's type |
|  3     |System sohw a message that this type is terminated |
|  4     |System does not leave you continue |
 
## Scenario 5

| Scenario ID: SC5        | Corresponds to UC:Handle sales (cash payment:there isn't quantity selected) |
| ------------- |:-------------:| 
|  1     |Manager log into LaTazza(has already recived the request and money)  |
|  2     |Select beverage's type |
|  3     |Select quantity |
|  4     |System sohw a message that there aren't enough capsules |
|  5     |System does not leave you continue |
 
## Scenario 6

| Scenario ID: SC6        | Corresponds to UC:Handle inventory (buy capsules) |
| ------------- |:-------------:| 
|  1     |Manager log into LaTazza  |
|  2     |Select beverage's type |
|  3     |Select quantity of boxes |
|  4     |Procede with payment|
|  5     |System send an e-mail to supplier|
|  6     |Order is saved on the order list |

## Scenario 7

| Scenario ID: SC7        | Corresponds to UC:Handle inventory (buy capsules: error in payment) |
| ------------- |:-------------:| 
|  1     |Manager log into LaTazza  |
|  2     |Select beverage's type |
|  3     |Select quantity of boxes |
|  4     |Procede with payment|
|  5     |System show that payment was not succesful|
|  6     |System abort operation|
|  7     |System return on main page|

## Scenario 8

| Scenario ID: SC8        | Corresponds to UC:Handle inventory (confirm order) |
| ------------- |:-------------:| 
|  1     | Manager log into LaTazza |
|  2     | Select order  |
|  3     | Store it |
|  4     | System update inventory |

## Scenario 9

| Scenario ID: SC9        | Corresponds to UC: Manage employee accounts |
| ------------- |:-------------:| 
|  1     | Manager log into LaTazza |
|  2     | Select Employee  |
|  3     | Write the amount |
|  4     | Procede with recharge |


# Glossary

![LaTazza Glossary](/templates/Glossary.png)
*Figure 1: Glossary*

# System Design
![LaTazza System Design](/templates/System Design.png)
*Figure 2: System Design*
