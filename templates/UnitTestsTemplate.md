# Unit Testing Documentation template

Authors: Matteo Borghesi, Angelo Floridia, Lorenzo Marzolla, Mattia Tarquinio

Date: May, 19th

Version: 1.0.0

# Contents

- [Black Box Unit Tests](#black-box-unit-tests)




- [White Box Unit Tests](#white-box-unit-tests)


# Black Box Unit Tests

    <Define here criteria, predicates and the combination of predicates for each function of each class.
    Define test cases to cover all equivalence classes and boundary conditions.
    In the table, report the description of the black box test case and the correspondence with the JUnit black box test case name/number>

 
### **Class *ReportManager* - method *getReport***

**Criteria for method *name*:**

 - startDate.compareTo(endDate)
 - transactions in time range

**Predicates for method *name*:**

| Criteria | Predicate |
| -------- | --------- |
|    startDate.compareTo(endDate)     |      > 0    |
|          |     =0       |
|          |       < 0    |
|    transactions in time range     |      =0   |
| | > 0 |

**Combination of predicates**:

| startDate.<br/>compareTo(endDate)| transactions in time range | Valid / Invalid | Description of the test case | JUnit test case |
|-------|-------|-------|-------|-------|
| 0 | 0 | yes | getReport(now,now) | ReportManagerTest.<br/>testGetReport |
| >0| 0 | no | getReport(later, now) | ReportManagerTest.<br/>testGetReport |
| <0 | 0 | yes | getReport(earlier, now)| ReportManagerTest.<br/>testGetReport |
| 0 | >0 | yes |  buyBoxes(0,0)<br/>d=getPurchases().head.date<br/>getReport(d,d)| ReportManagerTest.<br/>testGetReport |
| >0| >0 | no | d=getDate()<br/>buyBoxes(0,0)<br/>getReport(now, d)| ReportManagerTest.<br/>testGetReport |
| <0 | >0 | yes |d=getDate()<br/>buyBoxes(0,0)<br/>getReport(d, now)| ReportManagerTest.<br/>testGetReport |


### **Class *Employee* - method *recordConsumption***

**Criteria for method *name*:**

 - Other consumptions recharged
 - Consumption already present
 - Consumption valid

**Predicates for method *name*:**

| Criteria | Predicate |
| -------- | --------- |
| Other consumptions recharged    |       = 0    |
|          |       > 0    |
| Consumption already present | yes|
| | no |
| Consumption valid | yes |
| | no |

**Boundaries**:

| Criteria | Boundary values |
| -------- | --------------- |
|  Other consumptions recharged       |    0 , 1    |

**Combination of predicates**:

| Other consumptions recharged | Consumption already present | Consumption valid | Valid / Invalid | Description of the test case | JUnit test case |
|-------|-------|-------|-------|-------|-------|
| 0 | no | no | no | record(null) | EmployeeTest.<br/>testConsumptions |
| 0 | no | yes| yes | record(c) | EmployeeTest.<br/>testConsumptions |
| 0 | yes | no| no | - | -|
| 0 | yes | yes| no | - | - |
| >0 | no | no | no | record(c)<br/>record(null) | EmployeeTest.<br/>testConsumptions |
| >0 | no | yes| yes | record(c1)<br/>record(c2)| EmployeeTest.<br/>testConsumptions|
| >0 | yes | no| no | -| -|
| >0 | yes | yes| yes | record(c1)<br/>record(c1) | EmployeeTest.<br/>testConsumptions|



### **Class *EmployeeManager* - method *getEmployeeById***

**Criteria for method *name*:**
	
- id valid	
- employee existing

**Predicates for method *name*:**

| Criteria | Predicate |
| -------- | --------- |
| id valid  |  yes |
| | no |
|     employee existing     |      yes  |
| | no |

**Combination of predicates**:

| Id valid | Employee existing | Valid / Invalid | Description of the test case | JUnit test case |
|-------|-------|-------|-------|-------|
| yes | no | no |id=createEmployee<br/>getEmployeeById(id+1) | EmployeeManagerTest.<br/>testGetEmployeeById |
| yes | yes | yes | id=createEmployee<br/>getEmployeeById(id) |EmployeeManagerTest.<br/>testGetEmployeeById|
| no | no | no | getEmployeeById(null) | EmployeeManagerTest.<br/>testGetEmployeeById|
| no | yes | no | - | - |

### *TransactionManager* - *sellCapsules*



**Criteria for method *sellCapsules*:**
	

 - number of capsules
 - pay with account
 - enough money on account






**Predicates for method *sellCapsules*:**

| Criteria | Predicate |
| -------- | --------- |
|   number of capsules       |      >0     |
|          |      <=0     |
|       pay with cash   |     yes      |
|          |     no      |
| enough money on account|yes|
||no|






**Boundaries**:

| Criteria | Boundary values |
| -------- | --------------- |
|    number of capsule       |          0           |




**Combination of predicates**:


| number of capsules | pay with cash| enough money on account| Valid / Invalid | Description of the test case | JUnit test case |
|-------|-------|-------|-------|-------|-------|
|>0|yes|yes|V|sellCapsule(ide,idb,10,true)|testSellCapsule()|
|||no|V|sellCapsule(ide,idb,10,true)|testSellCapsule()|
||no|yes|V|sellCapsule(ide,idb,10,false)|testSellCapsule()|
|||no|V|sellCapsule(ide,idb,10,false)|testSellCapsule()|
|<0|yes|yes|I|sellCapsule(ide,idb,-10,true)-->ERR||
|||no|I|sellCapsule(ide,idb,-10,true)-->ERR||
||no|yes|I|sellCapsule(ide,idb,-10,false)-->ERR||
|||no|I|sellCapsule(ide,idb,-10,false)-->ERR||

### *TransactionManager* - *sellCapsulestoVisitors*


**Criteria for method *sellCapsulesToVisitors*:**


 - number of capsules
 - enough capsule in inventory
 

**Predicates for method *sellCapsulesToVisitors*:**


| Criteria | Predicate |
| -------- | --------- |
|   number of capsules       |      >0     |
|          |      <=0     |
|enough capsule in inventory|yes|
||no|





**Boundaries**:

| Criteria | Boundary values |
| -------- | --------------- |
|     number of capsules     |         0        |




**Combination of predicates**:


| number of capsules  |enough capsule in inventory |Valid / Invalid | Description of the test case | JUnit test case |
|-------|-------|-------|-------|-------|
|>0|yes|V|sellCapsulestoVisitor(idb,10)|testsellCapsulestoVisitor()|
||no|I|sellCapsulestoVisitor(idb,10)|testsellCapsulestoVisitor()|
|<0|yes|I|sellCapsulestoVisitor(idb,10)-->ERR||
||no|I|sellCapsulestoVisitor(idb,10)-->ERR||

### *InventoryManager* - *Update Capsule*



**Criteria for *Update Capsule*:**
	

 - Sign of id
 - Id already present





**Predicates for *Update Capsule*:**

| Criteria | Predicate |
| -------- | --------- |
|   Sign of id       |      >0     |
|          |    <0       |
|    Id already present      |      yes     |
|          |    no       |








**Combination of predicates**:


|Sign of id |Id already present| Valid / Invalid | Description of the test case | JUnit test case |
|-------|-------|-------|-------|-------|
|>0|yes|I|UpdateCapsule(1,"arabica",50,3)|testUpdateCapsule()|
||no|V|UpdateCapsule(2,"arabica",50,3)|testUpdateCapsule()|
|<0|yes|I|UpdateCapsule(-1,"arabica",50,3)|testUpdateCapsule()|
||no|I|UpdateCapsule(-2,"arabica",50,3))|testUpdateCapsule()|



### *InventoryManager* - *CreateBeverage*



**Criteria for method *CreateBeverage*:**
	

 - number of beverages created
 - beverage already exist
 
 





**Predicates for method *CreateBeverage***

| Criteria | Predicate |
| -------- | --------- |
|   number of beverages created       |     1      |
|          |    >1       |
|    beverage already exist      |     no      |
|          |    yes       |









**Combination of predicates**:


| number of beverages created|beverage already exist  | Valid / Invalid | Description of the test case | JUnit test case |
|-------|-------|-------|-------|-------|
|1|no|V|CreateBeverage("arabica",50,3)|testCreateBeverage()|
||yes|V|CreateBeverage("arabica",50,3)|testCreateBeverage()|
|>1|no|V|CreateBeverage("arabica",50,3) CreateBeverage("espresso",50,2) |testCreateBeverage()|
||yes|V|CreateBeverage("arabica",50,3) CreateBeverage("arabica",50,3) |testCreateBeverage()|



# White Box Unit Tests

### Test cases definition

    <Report here all the created JUnit test cases, and the units/classes they test >


| Unit name | JUnit test case |
|--|--|
| Beverage | BeverageTest.testBeverage	|
| BoxPurchase| BoxPurchaseTest.testGetSet |
| Consumption | ConsumptionTest.testGetSet |
| 			| ConsumptionTest.testToString |
| DataImpl	| DataImplTest.testFacade |
|EmployeeManager | EmployeeManagerTest.<br/>testCreateEmployee |
|				|  EmployeeManagerTest.<br/>testUpdateEmployee |
|				|  EmployeeManagerTest.<br/>testGetSet |
|				|  EmployeeManagerTest.<br/>testRecordConsumption |
|				|  EmployeeManagerTest.<br/>testRecordRecharge |
|				|  EmployeeManagerTest.<br/>testGetEmployeeById |
|				|  EmployeeManagerTest.<br/>testGetEmployeesId |
|				|  EmployeeManagerTest.<br/>testGetEmployees |
| Employee		| EmployeeTest.testGetSet |
|				| EmployeeTest.testConsumptions |
|				| EmployeeTest.testRecharges |
|				| EmployeeTest.testToString |
| InventoryManager | InventoryManagerTest.<br/>testCreateBeverage |
| 				| InventoryManagerTest.<br/>testUpdateBeverage |
| 				| InventoryManagerTest.<br/>testGetBeverageName |
| 				| InventoryManagerTest.<br/>testGetBeveragCapsulesPerBox |
| 				| InventoryManagerTest.<br/>testGetBeverageBoxPrice |	
| 				| InventoryManagerTest.<br/>testGetBeveragesId |	
| 				| InventoryManagerTest.<br/>testGetBeverages |
| 				| InventoryManagerTest.<br/>testGetSetCapsules |
| 				| InventoryManagerTest.<br/>testGetBeverageByPrice |
| 				| InventoryManagerTest.<br/>testGetCapsulePrice |
| LaTazza			| LaTazzaTest.testGetInstance |
|				| LaTazzaTest.testGetSet |
|				| LaTazzaTest.testReset |
| Recharge		| RechargeTest.testGetSet |
|				| RechargeTest.testToString |
| ReportManager	| ReportManagerTest.<br/>testGetReport |
|				| ReportManagerTest.<br/>testGetEmployeeReport |
| TransactionManager | TransactionManagerTest.<br/>testSellCapsules |
|				| TransactionManagerTest.<br/>testSellCapsulesToVisitor |
|				| TransactionManagerTest.<br/>testRechargeAccount |
|				| TransactionManagerTest.<br/>testBuyBoxes|
| Transaction		| TransactionTest.testGetSet |





### Code coverage report

<img src="codecoverage.png">

<img src="branchcoverage.png">



### Loop coverage analysis

    <Identify significant loops in the units and reports the test cases
    developed to cover zero, one or multiple iterations >

|Unit name | Loop rows | Number of iterations | JUnit test case |
|---|---|---|---|
| EmployeeManager | 118-121 | 0 | DataImplTest.testFacade |
|				  |		  | 1|  DataImplTest.testFacade |
|				  |		  | 2 | EmployeeManagerTest.<br/>testGetEmployees |
| ReportManager	 | 23-31 |	0 | DataImplTest.testFacade |
|				|	    |1  |  ReportManagerTest.<br/>testGetEmployeeReport |
|				|	    |2  |  DataImplTest.<br/>testFacade |
| ReportManager | 54-61 | 0 | DataImplTest.testFacade |
|					| | 1 | ReportManagerTest.<br/>testGetReport |





