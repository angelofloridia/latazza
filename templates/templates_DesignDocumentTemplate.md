# Design Document Template

Authors:

Date:

Version:

# Contents

- [Package diagram](#package-diagram)
- [Class diagram](#class-diagram)
- [Verification traceability matrix](#verification-traceability-matrix)
- [Verification sequence diagrams](#verification-sequence-diagrams)

# Instructions

The design document has to comply with:
1. [Official Requirement Document](../Official\ Requirements\ Document.md)
2. [DataInterface.java](../src/main/java/it/polito/latazza/data/DataInterface.java)

UML diagrams **MUST** be written using plantuml notation.

# Package diagram

\<define UML package diagram >

\<explain rationales for choices> 

\<mention architectural patterns used, if any>


# Class diagram

\<for each package define class diagram with classes defined in the package>

\<mention design patterns used, if any>

```plantuml

class LaTazza << (S, #FF7700) Singleton >>{
    - balance
    - LaTazza()
    + GetInstance()
    + GetInventoryManager()
    + GetEmployeeManager()
    + GetBalance()
    + Reset()
}

class EmployeeManager{
    - EmployeeManager()
    + CreateEmplyee()
    + UpdateEmployee()
    + GetEmployeeName()
    + GetEmployeeSurname()
    + GetEmployeeBalance()
    + GetEmployeesId()
    + GetEmployees()
}

class InventoryManager{
    + CreateBeverage()
    + UpdateBeverage()
    + GetBeverageName()
    + GetBeverageCapsulePerBox()
    + GetBeverageBoxPrice()
    + GetBeveragesId()
    + GetBeverages()
    + GetBeverageCapsules()
}

class TransactionManager{
    + SellCapsules()
    + SellCapsulesToVisitor()
    + RechargeAccount()
    + BuyBoxes()
}

class ReportManager{
    + GetEmployeeReport()
    + GetReport()
}

class Employee{
    - id
    - name
    - surname
    - balance
    - Employee()
    + GetName()
    + SetName()
    + GetSurname()
    + SetSurname()
    + GetBalance()
    + SetBalance()
    + GetId()
}

class Capsule{
}

class Beverage{
    - id
    - name
    - boxPrice
    - capsulePerBox
    - Beverage()
    + GetId()
    + GetName()
    + SetName()
    + GetBoxPrice()
    + SetBoxPrice()
    + SetCapsulePerBox()
    + GetCapsulePerBox()
}


class Transaction{
    - id
    - date
    - amount
    + GetId()
    + GetDate()
    + SetDate()
    + GetAmount()
    + SetAmount()
}

class Recharge{
}

class Consumption{
    - cashFlag
    + GetCashFlag()
    + SetCashFlag()
}

class BoxPurchase{
    - quantity
    + GetQuantity()
    + SetQuantity()
}

class DataInterface<< (I, orchid)>> {
     + CreateEmplyee()
    + UpdateEmployee()
    + GetEmployeeName()
    + GetEmployeeSurname()
    + GetEmployeeBalance()
    + GetEmployeesId()
    + GetEmployees()
    + CreateBeverage()
    + UpdateBeverage()
    + GetBeverageName()
    + GetBeverageCapsulePerBox()
    + GetBeverageBoxPrice()
    + GetBeveragesId()
    + GetBeverages()
    + GetBeverageCapsules()
    + SellCapsules()
    + SellCapsulesToVisitor()
    + RechargeAccount()
    + BuyBoxes()
    + GetEmployeeReport()
    + GetReport()
    + GetBalance()
    + Reset()
}

class DataImpl{
}

LaTazza --> "1" EmployeeManager
LaTazza --> "1" InventoryManager
DataInterface --> "1" EmployeeManager
DataInterface --> "1" InventoryManager
DataInterface --> "1" TransactionManager
DataInterface --> "1" ReportManager
DataImpl ..|> DataInterface
EmployeeManager --> "'0..*" Employee
Employee --> "0..*" Recharge
Employee --> "0..*" Consumption
Recharge --> "1" Employee
Consumption --> "1" Employee
BoxPurchase --> "1" Beverage
BoxPurchase --|> Transaction
Consumption --|> Transaction
Recharge --|> Transaction
InventoryManager --> "0..*" Capsule
InventoryManager --> "0..*" Beverage
Capsule --> "1" Beverage
Consumption --> "1..*" Capsule
TransactionManager --> "0..*" Transaction

@enduml
```


# Verification traceability matrix

\<for each functional requirement from the requirement document, list which classes concur to implement it>


|  | Class x | Class y  | .. |
| ------------- |:-------------:| -----:| -----:|
| Functional requirement x  |  |  | |
| Functional requirement y  |  |  | |
| .. |  |  | |

# Verification sequence diagrams 
\<select key scenarios from the requirement document. For each of them define a sequence diagram showing that the scenario can be implemented by the classes and methods in the design>

```plantuml
": Class X" -> ": Class Y": "1: message1()"
": Class X" -> ": Class Y": "2: message2()"
```