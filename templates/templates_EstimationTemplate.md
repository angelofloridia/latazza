# Project Estimation  template

Authors: Matteo Borghesi, Angelo Floridia, Lorenzo Marzolla, Mattia Tarquinio

Date: 2/6/2019

Version: v1.0.0

# Contents

- [[Data from your LaTazza project]

- [Estimate by product decomposition]
- [Estimate by activity decomposition ]



# Data from your LaTazza project

###
|||
| ----------- | ------------------------------- | 
|         Total person hours  worked by your  team, considering period March 5 to May 26, considering ALL activities (req, des, code, test,..)    | 280  |             
|Total Java LoC delivered on May 26 (only code, without Exceptions, no Junit code) | 1600 |
| Total number of Java classes delivered on May 26 (only code, no Junit code, no Exception classes)| 18 |
| Productivity P =| 6 LOC/ph or 46 LOC/day |
|Average size of Java class A = | 89 LOC|

# Estimate by product decomposition



### 

|             | Estimate                        |             
| ----------- | ------------------------------- |  
| Estimated n classes NC (no Exception classes)  |    18                         |             
| Estimated LOC per class  (Here use Average A computed above )      |         89                   | 
| Estimated LOC (= NC * A) | 1600|
| Estimated effort  (person days) (Here use productivity P)  |   35                                  |      
| Estimated calendar time (calendar weeks) (Assume team of 4 people, 8 hours per day, 5 days per week ) |          1.75 weeks         |               


# Estimate by activity decomposition



### 

|         Activity name    | Estimated effort (PD)   |             
| ----------- | ------------------------------- | 
| Requirement elicitation | 2 |
| Requirement specification | 5 |
| Requirement validation | 2 |
| Architectural design | 1 |
| Component design | 3 |
| Design validation | 1 |
| Implementation | 7 |
| Unit testing | 2 |
| Integration testing | 3 |
| System testing | 2 |
| Planning | 2 |
| Tracking | 1 |
| Managing risks | 1 |
| Configuration management | 3 |


###
<img src="images_Gantt.png">

